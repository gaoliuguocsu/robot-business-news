package centaurstech.ai.robot.server.news.pojo.api;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ToString
@Data
public class LetingProvince {

    private String province;

    public LetingProvince(){}

    public LetingProvince(String province){
        this.province = province;
    }

}
