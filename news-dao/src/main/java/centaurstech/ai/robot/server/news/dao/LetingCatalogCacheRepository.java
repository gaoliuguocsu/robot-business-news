package centaurstech.ai.robot.server.news.dao;

import centaurstech.ai.robot.server.news.pojo.api.LetingCatalog;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LetingCatalogCacheRepository extends MongoRepository<LetingCatalog, String> {

    LetingCatalog findOneByCatalogId(String catalogId);

    LetingCatalog findOneByCatalogName(String catalogName);

    void deleteByCatalogId(String catalogId);

}
