package centaurstech.ai.robot.server.news.strategy.base;

import centaurstech.ai.robot.server.common.result.QueryResult;
import centaurstech.ai.robot.server.news.lazyload.LazyLoadHelper;
import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.AbstractStrategyRouter;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import com.centaurstech.domain.FormRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public abstract class BaseStrategyRouter extends AbstractStrategyRouter<NewsQuery, NewsCache, StrategyHandlerName, CommonResult> {


    protected ObjectMapper objectMapper;



    protected LazyLoadHelper<Map<StrategyHandlerName, StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult>>> handlersMap = new LazyLoadHelper<>(()->{
        Map<String, StrategyHandler> beanNamesForType = getApplicationContext().getBeansOfType(StrategyHandler.class);
        return beanNamesForType.entrySet().stream().map(Map.Entry::getValue).map(v -> (StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult>) v).collect(Collectors.toMap(StrategyHandler::getHandlerName, s -> s));
    },"懒加载所有类型处理器");

    protected LazyLoadHelper<List<StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult>>> preHandlers = new LazyLoadHelper<>(()->{
        List<StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult>> preHandlers = handlersMap.getData().entrySet().stream().map(Map.Entry::getValue).filter(v -> v.getType() == StrategyHandlerType.PRE).distinct().sorted(Comparator.comparingInt(StrategyHandler::getOrder)).collect(Collectors.toList());
        String orderStr = preHandlers.stream().map(h -> h.getClass().getSimpleName()).collect(Collectors.joining(","));
        log.info("pre类型的加载器顺序：[{}]", orderStr);
        return preHandlers;
    },"懒加载筛选PRE类型的处理器");

    protected LazyLoadHelper<List<StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult>>>  lastHandlers = new LazyLoadHelper<>(()->{
        List<StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult>> lastHandlers = handlersMap.getData().entrySet().stream().map(Map.Entry::getValue).filter(v -> v.getType() == StrategyHandlerType.LAST).distinct().sorted(Comparator.comparingInt(StrategyHandler::getOrder)).collect(Collectors.toList());
        String orderStr = lastHandlers.stream().map(h -> h.getClass().getSimpleName()).collect(Collectors.joining(","));
        log.info("last类型的处理器顺序：[{}]", lastHandlers);
        return lastHandlers;
    },"懒加载筛选LAST类型的处理器");

    /**
     * 根据名字获取handler
     * @param handlerName
     * @return
     */
    protected StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult> getSingleStartegyHandler(StrategyHandlerName handlerName){
        return handlersMap.getData().get(handlerName);
    }


}
