package centaurstech.ai.robot.server.news.strategy.pre;

import centaurstech.ai.robot.server.common.result.QueryResult;
import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.api.LetingCatalog;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import centaurstech.ai.robot.server.news.utils.DateUtils;
import com.centaurstech.domain.FormRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class UserBehaviorHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.PRE_USER_BEHAVIOR;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.PRE;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("用户行为偏好处理...");

        String negate = params.get(FormVarName.VARS_NEGATE);
        if (StringUtils.isNotEmpty(negate)){
            return StrategyResult.toNextPre(params, cache);
        }

        //用户行为记录
        StrategyResult<NewsQuery, NewsCache, StrategyHandlerName> result = null;
        String categoryName = params.get(FormVarName.VARS_NEWS_CATEGORY);
        if (StringUtils.isNotEmpty(categoryName)) {
            String[] cates = categoryName.split("\\+");
            for (String c : cates) {
                LetingCatalog catalog = letingService.getCatalogByName(c);
                if (catalog != null) {
                    //用户行为记录, 类别权重+1
                    userBehaviorService.increaseCatalog(params.get(FormVarName.UID), catalog.getCatalogName());
                }
            }

        }

        String news = params.get(FormVarName.VARS_NEWS);
        if (StringUtils.isNotEmpty(news)){
            //用户行为记录, 时间段权重+1
            userBehaviorService.increasePeriod(params.get(FormVarName.UID));
        }


        return StrategyResult.toNextPre(params, cache);
    }
}
