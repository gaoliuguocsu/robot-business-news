package centaurstech.ai.robot.server.news.strategy.abstracts;

import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import org.springframework.aop.framework.AopContext;

import java.io.IOException;

/**
 * @author 
 * @date 
 */
public interface StrategyHandler<P, C, N, R> {


    N getHandlerName();

    /**
     * 此处type应该是个泛型的。。但是已经用上了，后面有其他需要抽象的再改吧
     * @return
     */
    StrategyHandlerType getType();

    /**
     * 根据业务需求，全部要改成可以排序的
     * @return
     */
    default int getOrder(){
        return 0;
    }
 
    /** 
     * apply strategy 
     * 
     * @param  params
     * @param  cache
     * @param
     * @return
     */ 
    StrategyResult<P, C, R> apply(P params, C cache) throws IOException;


    /**
     * 默认的重载方法，方便操作
     * @param result
     * @return
     * @throws IOException
     */
    default StrategyResult<P, C, R> apply(StrategyResult<P, C, R> result) throws IOException{
        //内部调用
        StrategyHandler proxyTarget = (StrategyHandler) (AopContext.currentProxy());
        return proxyTarget.apply(result.getParams(), result.getCache());
    }


    @SuppressWarnings("rawtypes")
    StrategyHandler DEFAULT = new StrategyHandler<>() {
        @Override
        public StrategyHandlerName getHandlerName() {
            return StrategyHandlerName.DEAFULT;
        }

        @Override
        public StrategyHandlerType getType() {
            return StrategyHandlerType.NORMAL;
        }

        @Override
        public StrategyResult apply(Object params, Object cache) {
            throw new RuntimeException("未能处理的情况");
        }
    };

}