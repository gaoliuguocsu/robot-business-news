package centaurstech.ai.robot.server.news.strategy.enums;


/**
 * TODO 此处后面需要改成按照顺序来排列
 */
public enum StrategyHandlerName {

    /**
     * default 占位
     */
    DEAFULT("默认，无作为"),
    /**
     * root
     */
    ROOT_HANDLER("根处理器"),
    /**
     * pre
     */
    PRE_OPERATE_NEWS("前置操作", 1),
    PRE_USER_BEHAVIOR("用户行为偏好", 2),
    SWITCH_NEWS("切换新闻", 3),
    NEWS_TIME_CONVERT("时间转换", 4),
    NEWS_AREA("区域", 5),



    /**
     * 区域新闻
     */


    NEWS_EVENT("事件"),
    NEWS_CATEGORY("新闻类别"),
    NEWS_PROGRAM("节目"),
    NEWS_NAME("人名"),
    NEWS_TIME("时间"),
    NEWS_EARLY_EVENING("早晚报"),
    NEWS_RECOMMEND("推荐"),


    /**
     * last
     */
    UPDATE_MEMORY("保存或更新记忆"),

    ;

    /**
     * 控制器的名字
     */
    private String name;
    /**
     * 控制器的顺序
     */
    private int order = 0;

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }

    StrategyHandlerName(String name) {
        this.name = name;
    }

    StrategyHandlerName(String name, int order) {
        this.name = name;
        this.order = order;
    }
}