package centaurstech.ai.robot.server.news.strategy;

import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.service.IntentionService;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyHandler;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyRouter;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Component
public class RootStrategyRouter extends BaseStrategyRouter implements StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult> {

    @Autowired
    private IntentionService intentionService;

    @Override
    protected StrategyMapper<NewsQuery, NewsCache, StrategyHandlerName, CommonResult> registerStrategyMapper() {
        return (query, cache) -> {
            StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult> handler = null;

            if(intentionService.hasEventIntention(query)){
                handler = getSingleStartegyHandler(StrategyHandlerName.NEWS_EVENT);
            }
            else if(intentionService.hasCategoryIntention(query)){
                handler = getSingleStartegyHandler(StrategyHandlerName.NEWS_CATEGORY);
            }
            else if(intentionService.hasProgramIntention(query)){
                handler = getSingleStartegyHandler(StrategyHandlerName.NEWS_PROGRAM);
            }
            else if(intentionService.hasNameIntention(query)){
                handler = getSingleStartegyHandler(StrategyHandlerName.NEWS_NAME);
            }
            else if(intentionService.hasTimeIntention(query)){
                handler = getSingleStartegyHandler(StrategyHandlerName.NEWS_TIME);
            }
            else if(intentionService.hasEarlyEveningIntention(query)){
                handler = getSingleStartegyHandler(StrategyHandlerName.NEWS_EARLY_EVENING);
            }
            else if(intentionService.hasRecommendIntention(query)){
                handler = getSingleStartegyHandler(StrategyHandlerName.NEWS_RECOMMEND);
            }

            //普通
            return Optional.of(handler).orElseThrow(()->new RuntimeException("找不到合适的handler"));
        };
    }

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.ROOT_HANDLER;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.ROOT;
    }


    /**
     * 流程入口
     * @param  params
     * @param  cache
     * @return
     * @throws IOException
     */
    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        //1、处理must类型的，如果有结果，返回，否则再分发
        for (StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult> mustHandler : preHandlers.getData()) {
            //must之间不传递otherData
            StrategyResult<NewsQuery, NewsCache, CommonResult> preResult = mustHandler.apply(params, cache);
            if(preResult.isDone()){
                log.info("must类型的handler[{}]处理完成，返回", mustHandler.getHandlerName());
                return preResult;
            }
        }

        //2、进行分发
        StrategyResult result  = applyStrategy(params, cache);
        return result;
    }


}
