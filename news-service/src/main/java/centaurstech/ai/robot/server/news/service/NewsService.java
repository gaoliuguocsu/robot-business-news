package centaurstech.ai.robot.server.news.service;

import centaurstech.ai.robot.server.common.result.QueryResult;
import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.RootStrategyRouter;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import com.centaurstech.domain.FormRequest;
import com.centaurstech.domain.FormResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class NewsService {

    @Autowired
    private RootStrategyRouter rootStrategyRouter;

    @Autowired
    private MemoryService memoryService;

    @Autowired
    private ObjectMapper objectMapper;


    public FormResponse.FormData<CommonResult, Map<String, String>> execute(FormRequest<Map<String, String>, Map<String, String>> request) throws Exception{

        CommonResult data = new CommonResult();

        try {
            //从缓存里获取记忆数据
            NewsCache cache = memoryService.getNewsCache(request.getChatKey());

            NewsQuery params = new NewsQuery(request);

            //主逻辑进行处理分发
            StrategyResult<NewsQuery, NewsCache, CommonResult> result = rootStrategyRouter.apply(params, cache);

            if (result.isDone()) {
                data = result.getCommonResult();
                if (data == null){
                    data = new CommonResult();
                }
            }

            data.setForm(objectMapper.writeValueAsString(request));

            data.setParams(request.getGroupVars());

        }catch (Exception e){
            log.debug("NewsService", e);
            data.setForm(objectMapper.writeValueAsString(request));
            data.setParams(request.getGroupVars());
            data.setAnswer("Bot服务缺少相应处理");
        }

        return new FormResponse.FormData<CommonResult, Map<String, String>>(data, null);
    }

}
