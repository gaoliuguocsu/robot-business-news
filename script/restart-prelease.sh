#!/bin/bash
set -e				# 遇到错误就退出

echo "Runing restart script ... User:${USER}"

appFolder="robot-business-navigation"
exeName="robot-business-navigation"
containerName="robot-business-navigation-pre"

# go to app folder
cd ${appFolder}
jarPath=$(find . -name "*.jar")

jarFileName=$(basename "$jarPath")
filename="${jarFileName%.*}"

# set jvm parameters
javaParams="-Xmx1024m -jar"

# set spring boot profile
springProfile="--spring.profiles.active=prelease"
echo "springProfile:${springProfile}"

# create soft link file
ln -sf ${jarPath} ./${exeName}

if [ ! "$(sudo docker ps -a -q -f name=${containerName})" ]; then
	# if no container
	sudo docker run --name ${containerName} -d\
	    --log-opt max-size=1g --log-opt max-file=50 \
		--net host \
		-v "$PWD":/usr/app -w /usr/app \
		adoptopenjdk/openjdk11 /bin/bash -c "java ${javaParams} ${exeName} ${springProfile}"
else
	# stop api docker images
	sudo docker ps -aq --no-trunc --filter "name=${containerName}" \
	| xargs sudo docker stop

	# start api docker images
	sudo docker ps -aq --no-trunc --filter "name=${containerName}" \
	| xargs sudo docker start
fi

