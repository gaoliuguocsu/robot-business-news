package centaurstech.ai.robot.server.news.strategy.base;

import centaurstech.ai.robot.server.common.result.QueryResult;
import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import com.centaurstech.domain.FormRequest;

import java.util.Map;

/**
 * @author yiklam
 * @description
 * @create 2021-05-06 14:11
 **/
public abstract class BaseHandlerAndRouter extends BaseStrategyRouter implements StrategyHandler<NewsQuery,  NewsCache, StrategyHandlerName, CommonResult> {
}
