package centaurstech.ai.robot.server.news.service;

import centaurstech.ai.robot.server.news.enums.NewsRedisTable;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.user.UserBehavior;
import com.centaurstech.redis.service.v2.CacheServiceV2;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author yiklam
 * @description
 * @create 2021-05-07 10:05
 **/
@Slf4j
@Service
public class MemoryService {


    private static Long DEFAULT_TIME_OUT = 1 * 3600 * 1000L;//默认缓存为 1个小时

    @Autowired
    private CacheServiceV2 cacheService;


    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 获得从缓存里面获得用户记忆
     *
     * @param chatKey
     * @return
     */
    public NewsCache getNewsCache(String chatKey) {
        NewsCache cache = queryOne(chatKey);
        if (cache == null) {
            cache = new NewsCache(chatKey);
        }
        return cache;
    }

    public NewsCache queryOne(String chatKey) {
        NewsCache cache = (NewsCache)cacheService.getObj(NewsRedisTable.NEWS_CACHE, chatKey);
        return cache;
    }

    public NewsCache saveCache(NewsCache cache){
        cacheService.setObj(NewsRedisTable.NEWS_CACHE, cache.getChatKey(), cache, DEFAULT_TIME_OUT);
        return cache;
    }

    public void removeUserCache(String chatKey) {
        cacheService.delKey(NewsRedisTable.NEWS_CACHE, chatKey);
    }

}
