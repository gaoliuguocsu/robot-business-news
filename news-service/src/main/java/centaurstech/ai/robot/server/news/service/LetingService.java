package centaurstech.ai.robot.server.news.service;

import centaurstech.ai.robot.server.common.response.ApiQueryResult;
import centaurstech.ai.robot.server.news.api.LetingClient;
import centaurstech.ai.robot.server.news.dao.LetingCatalogCacheRepository;
import centaurstech.ai.robot.server.news.dao.LetingCityCacheRepository;
import centaurstech.ai.robot.server.news.dao.LetingProvinceCacheRepository;
import centaurstech.ai.robot.server.news.pojo.api.LetingCatalog;
import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.api.LetingCity;
import centaurstech.ai.robot.server.news.pojo.api.LetingProvince;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class LetingService {

    @Autowired
    private LetingClient letingClient;


    @Autowired
    private LetingCatalogCacheRepository letingCatalogCacheRepository;

    @Autowired
    private LetingProvinceCacheRepository letingProvinceCacheRepository;

    @Autowired
    private LetingCityCacheRepository letingCityCacheRepository;


    private Map<String, LetingCatalog> catalogMap = null;


    /**
     * 拉取 乐听 类别数据， 并存入 临时MAP
     */
    public boolean pullCatalogs(){

        ApiQueryResult result = letingClient.newsCatalogs();
        if (result != null && result.getRetcode() == 0){
            JSONArray array = JSON.parseArray(JSONArray.toJSONString(result.getData()));
            if (array != null && array.size() > 0) {
                catalogMap = new HashMap<String, LetingCatalog>();
                for (int i = 0; i < array.size(); i++) {
                    LetingCatalog catalog = new LetingCatalog(array.getJSONObject(i));
                    catalogMap.put(catalog.getCatalogName(), catalog);
                    letingCatalogCacheRepository.save(catalog);
                }
                return true;
            }
        }
        return false;
    }


    /**
     * 加载 乐听 类别数据 并存入 临时MAP
     * @return
     */
    public boolean loadCatalogs(){
        List<LetingCatalog> list = letingCatalogCacheRepository.findAll();
        if (list != null && list.size() > 0) {
            catalogMap = new HashMap<String, LetingCatalog>();
            for (LetingCatalog bean : list) {
                catalogMap.put(bean.getCatalogName(), bean);
            }
            return true;

        }

        return false;
    }


    /**
     * 拉取 乐听 省市数据
     */
    public boolean pullRegions(){

        ApiQueryResult result = letingClient.newsRegions();
        if (result != null && result.getRetcode() == 0){
            JSONObject json = JSON.parseObject(JSONObject.toJSONString(result.getData()));
            if (json != null) {
                for (String key : json.keySet()) {
                    JSONObject province = json.getJSONObject(key);
                    if (province != null){
                        String pName = province.getString("name");
                        LetingProvince pBean = new LetingProvince(pName);
                        letingProvinceCacheRepository.save(pBean);

                        JSONObject cityJson = province.getJSONObject("children");
                        for (String ckey : cityJson.keySet()){
                            JSONObject city = cityJson.getJSONObject(ckey);
                            String cName = city.getString("name");
                            LetingCity cBean = new LetingCity(pName, cName);
                            letingCityCacheRepository.save(cBean);

                        }
                    }
                }
                return true;
            }
        }
        return false;
    }


    public LetingCity getCityByName(String name){

        Long count = letingCityCacheRepository.count();
        if (count == 0){
            pullRegions();
        }

        return letingCityCacheRepository.findOneByCityLike(name);
    }

    public LetingProvince getProvinceByName(String name){
        Long count = letingProvinceCacheRepository.count();
        if (count == 0){
            pullRegions();
        }

        return letingProvinceCacheRepository.findOneByProvinceLike(name);
    }

    /**
     * 根据 类别名获得类别 信息
     * @param name
     * @return
     */
    public LetingCatalog getCatalogByName(String name){
        name = name.replace("新闻", "").replace("资讯", "").replace("类", "").replace("频道", "").replace("界", "");
        if (catalogMap != null){
            return catalogMap.get(name);
        }


        Long count = letingCatalogCacheRepository.count();
        if (count == 0){
            pullCatalogs();
        }

        if(loadCatalogs()) {
            return catalogMap.get(name);
        }

        return null;
    }

    /**
     * 排除某些类别
     * @param catalogs
     * @return
     */
    public List<LetingCatalog> excludeCatalog(String[] catalogs){
        if (catalogMap == null){
            Long count = letingCatalogCacheRepository.count();
            if (count == 0){
                pullCatalogs();
            }
        }

        if(loadCatalogs()) {
            List<LetingCatalog> result = new ArrayList<>();
            List<String> list = Arrays.asList(catalogs);
            for(String key : catalogMap.keySet()){
                if (list.contains(key)){
                    continue;
                }
                result.add(catalogMap.get(key));
            }
            return result;
        }

        return null;
    }

    /**
     * 指定频道 新闻
     * @param catalogId
     * @param keyword
     * @return
     */
    public List<LetingNews> newsChannel(String catalogId, String keyword, String startTime, String endTime, String province, String city){

        log.debug("newsChannel=======catalogId=" + catalogId + "&keyword=" + keyword + "&startTime=" + startTime + "&endTime=" + endTime);
        ApiQueryResult result = letingClient.newsChannel(catalogId, keyword, startTime, endTime, province, city);
        if (result != null && result.getRetcode() == 0){
            JSONObject json = JSON.parseObject(JSONObject.toJSONString(result.getData()));
            if (json != null && json.getInteger("count") > 0) {
                JSONArray array = json.getJSONArray("data");
                List<LetingNews> list = new ArrayList<LetingNews>();
                for (int i = 0; i < array.size(); i++) {
                    LetingNews bean = new LetingNews(array.getJSONObject(i));
                    list.add(bean);
                }
                return list;
            }
        }

        return null;
    }

    /**
     * 获取推荐新闻
     * @param province
     * @param city
     */
    public List<LetingNews> recommend(String province, String city){

        log.debug("recommend ======= province=" + province + "&city=" + city);
        ApiQueryResult result = letingClient.newsRec(province, city);
        if (result != null && result.getRetcode() == 0){
            JSONObject json = JSON.parseObject(JSONObject.toJSONString(result.getData()));
            if (json != null && json.getInteger("count") > 0) {
                JSONArray array = json.getJSONArray("data");
                List<LetingNews> list = new ArrayList<LetingNews>();
                for (int i = 0; i < array.size(); i++) {
                    LetingNews bean = new LetingNews(array.getJSONObject(i));
                    list.add(bean);
                }
                return list;
            }
        }

        return null;
    }


    /**
     * 搜索新闻
     * @param keyword
     * @param source
     * @param publishTime
     * @param province
     * @param city
     * @return
     */
    public List<LetingNews> search(String keyword, String source, String publishTime, String province, String city){

        log.debug("search ======= keyword=" + keyword + "&source=" + source + "&publishTime=" + publishTime + "&province=" + province + "&city=" + city);
        ApiQueryResult result = letingClient.search(keyword, source, publishTime, province, city);
        if (result != null && result.getRetcode() == 0){
            JSONObject json = JSON.parseObject(JSONObject.toJSONString(result.getData()));
            if (json != null && json.getInteger("count") > 0) {
                JSONArray array = json.getJSONArray("data");
                List<LetingNews> list = new ArrayList<LetingNews>();
                for (int i = 0; i < array.size(); i++) {
                    LetingNews bean = new LetingNews(array.getJSONObject(i));
                    list.add(bean);
                }
                return list;
            }
        }

        return null;

    }

    /**
     * 查询
     * @param query
     * @return
     */
    public List<LetingNews> query(String query){

        log.debug("query ======= query=" + query);
        ApiQueryResult result = letingClient.newsQuery(query);
        if (result != null && result.getRetcode() == 0){
            JSONObject json = JSON.parseObject(JSONObject.toJSONString(result.getData()));
            if (json != null && json.getInteger("count") > 0) {
                JSONArray array = json.getJSONArray("data");
                List<LetingNews> list = new ArrayList<LetingNews>();
                for (int i = 0; i < array.size(); i++) {
                    LetingNews bean = new LetingNews(array.getJSONObject(i));
                    list.add(bean);
                }
                return list;
            }
        }

        return null;
    }

    /**
     * 早晚报
     * @param catalogId   catalog_id目前只支持：社会(综合类新闻），科技，财经，娱乐，汽车对应的频道id，默认返回社会(综合类)新闻
     * @param province
     * @param city
     * @param type
     * @return
     */
    public List<LetingNews> newsBrief(String catalogId, String province, String city, Integer type){

        log.debug("newsBrief ======= catalogId=" + catalogId +"&province=" + province + "&city=" + city + "&type=" + type);
        ApiQueryResult result = letingClient.newsBrief(catalogId, province, city, type);
        if (result != null && result.getRetcode() == 0){
            JSONObject json = JSON.parseObject(JSONObject.toJSONString(result.getData()));
            if (json != null && json.getInteger("count") > 0) {
                JSONArray array = json.getJSONArray("data");
                List<LetingNews> list = new ArrayList<LetingNews>();
                for (int i = 0; i < array.size(); i++) {
                    LetingNews bean = new LetingNews(array.getJSONObject(i));
                    list.add(bean);
                }
                return list;
            }
        }

        return null;

    }




}
