package centaurstech.ai.robot.server.news.strategy.normal;

import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.NewsEntity;
import centaurstech.ai.robot.server.news.pojo.NewsResponse;
import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.constants.NewsContants;
import centaurstech.ai.robot.server.news.pojo.enums.ResultCode;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Slf4j
@Component
public class EarlyEveningNewsHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.NEWS_EARLY_EVENING;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.NORMAL;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("早晚报处理器...");

        StrategyResult<NewsQuery, NewsCache, CommonResult> result = null;

        //早晚报
        String earlyEvening = params.get(FormVarName.VARS_EARLY_EVENING);
        if (StringUtils.isNotEmpty(earlyEvening)) {

            String province = params.get(FormVarName.PARAM_PROVINCE) == null? "" : params.get(FormVarName.PARAM_PROVINCE);
            String city = params.get(FormVarName.PARAM_CITY) == null? "" : params.get(FormVarName.PARAM_CITY);
            Integer type = 1;
            if(NewsContants.EVENING_NEWS.equals(earlyEvening)){
                type = 2;
            }

            List<LetingNews> list = letingService.newsBrief("", province, city, type);
            if (list != null) {

                //保存缓存
                Integer index = 0;
                cache.setIndex(index);
                cache.setNewList(list);
                memoryService.saveCache(cache);

                //将数据直接返回中控
                NewsResponse respObj = new NewsResponse(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), NewsEntity.convert(list));
                String s = objectMapper.writeValueAsString(respObj);
                String ticket =  chatApiService.sendNewsList(new JSONObject(s));

                String newsStr = generateNews(list);
//                String newsStr = list.get(index).getTitle();
                CommonResult commonResult = new CommonResult( NewsContants.FOUNDED_NEWS, newsStr, ticket);
                result = StrategyResult.done(cache, commonResult);
                return result;
            }

        }

        //将数据直接返回中控
//        NewsResponse respObj = new NewsResponse(ResultCode.EMPTY.getCode(), ResultCode.EMPTY.getMsg());
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("data",respObj);
//        chatApiService.sendNewsList(jsonObject);

        CommonResult commonResult = new CommonResult("", "", NewsContants.NOT_FOUND_NEWS, "");
        result = StrategyResult.done(cache, commonResult);
        return result;

    }
}
