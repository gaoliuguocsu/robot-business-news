package centaurstech.ai.robot.server.news.strategy.base;


import centaurstech.ai.robot.server.common.result.QueryResult;
import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import com.centaurstech.domain.FormRequest;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Setter
@Slf4j
public abstract class BaseStrategyHandler extends BaseServiceHolder implements StrategyHandler<NewsQuery, NewsCache, StrategyHandlerName, CommonResult> {

    @Override
    public int getOrder() {
        return getHandlerName().getOrder();
    }

    public String generateNews(List<LetingNews> list){
        StringBuffer sb = new StringBuffer();
        int index = 1;
        for (LetingNews bean : list){
            sb.append(index++ + ".").append(bean.getTitle());
        }

        return sb.toString();
    }
}
