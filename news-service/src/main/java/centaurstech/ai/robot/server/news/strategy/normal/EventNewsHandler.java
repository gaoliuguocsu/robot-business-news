package centaurstech.ai.robot.server.news.strategy.normal;

import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.NewsEntity;
import centaurstech.ai.robot.server.news.pojo.NewsResponse;
import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.constants.NewsContants;
import centaurstech.ai.robot.server.news.pojo.enums.ResultCode;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;

@Slf4j
@Component
public class EventNewsHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.NEWS_EVENT;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.NORMAL;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("事件新闻处理器...");

        StrategyResult<NewsQuery, NewsCache, CommonResult> result = null;

        // 事件搜索
        String event = params.get(FormVarName.VARS_EVENT);
        if (StringUtils.isNotEmpty(event)) {

            String eventTime = params.get(FormVarName.PARAM_TIME_DATE) == null ? "" : params.get(FormVarName.PARAM_TIME_DATE);
            String province = params.get(FormVarName.PARAM_PROVINCE) == null? "" : params.get(FormVarName.PARAM_PROVINCE);
            String city = params.get(FormVarName.PARAM_CITY) == null? "" : params.get(FormVarName.PARAM_CITY);
            String source = "";
            String area = params.get(FormVarName.PARAM_AREA) == null? "" : params.get(FormVarName.PARAM_AREA);

            if (StringUtils.isNotEmpty(area)){
                event = area + event;
            }


            List<LetingNews> list = null;
            String negate = params.get(FormVarName.VARS_NEGATE);
            if (StringUtils.isNotEmpty(negate)){
               //推荐新闻
               list = letingService.recommend("", "");

            }else {
                list = letingService.search(event, source, eventTime, province, city);
            }
            if (list != null) {

                //保存缓存
                cache.setNewsQuery(params);
                Integer index = 0;
                cache.setIndex(index);
                cache.setNewList(list);
                memoryService.saveCache(cache);

                //将数据直接返回中控
                NewsResponse respObj = new NewsResponse(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), NewsEntity.convert(list));
                String s = objectMapper.writeValueAsString(respObj);
                String ticket =  chatApiService.sendNewsList(new JSONObject(s));

                String newsStr = generateNews(list);
//                String newsStr = list.get(index).getTitle();
                CommonResult commonResult = new CommonResult( NewsContants.FOUNDED_NEWS, newsStr, ticket);
                result = StrategyResult.done(cache, commonResult);
                return result;
            }else{
                //保存缓存
                cache.setNewsQuery(params);
                memoryService.saveCache(cache);

                //将数据直接返回中控
//                NewsResponse respObj = new NewsResponse(ResultCode.EMPTY.getCode(), ResultCode.EMPTY.getMsg());
//                String s = objectMapper.writeValueAsString(respObj);
//                String ticket =  chatApiService.sendNewsList(new JSONObject(s));

                String answer = MessageFormat.format(NewsContants.NOT_FOUND_NEWS, event);
                CommonResult commonResult = new CommonResult("", "", answer, "");
                result = StrategyResult.done(cache, commonResult);
                return result;
            }

        }


        result = StrategyResult.toNextPre(params, cache);
        return result;
    }
}
