package centaurstech.ai.robot.server.news;


import com.alibaba.fastjson.JSONObject;
import com.centaurstech.domain.FormResponse;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class NewsTest extends BaseTest{


    String A_KEY = generateChatKey();

    @Test
    public void testNews() throws Exception {

        Map<String,String> p1 = new HashMap<String, String>();
        p1.put("操作", "听");
        p1.put("时间甲", "昨天");
        p1.put("新闻", "新闻");

        List<Map> groupVars = new ArrayList<>();
        groupVars.add(p1);

        Map<String, String> vars = new HashMap<>();
        vars.put("操作", "听+听+听");
        vars.put("时间甲", "昨天+昨天+昨天");
        vars.put("新闻", "新闻+新闻");
        vars.put("CURRENT_UPDATE_IN_ADVANCE_VARS", "操作+时间甲+新闻");

        Map<String, Object> params = new HashMap<>();

        params.put("chat_key", A_KEY);
        params.put("appVersion", "");
        params.put("botAccount", "");
        params.put("uid", "");
        params.put("query_text", "我想操作甲时间甲的新闻");
        params.put("originText", "我想听昨天的新闻");
        params.put("vars", vars);
        params.put("groupVars", groupVars);


        ResultActions reaction = mockMvc
                .perform(post("/api/form/news/")
                        .contentType(APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(JSONObject.toJSONString(params)))
                .andDo(MockMvcResultHandlers.print());  // 控制台打印出请求体

        reaction.andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult mvcResult =reaction.andReturn();
//        System.out.println(mvcResult.getResponse().getContentAsString());

        FormResponse response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), FormResponse.class);

        Map res = (Map) response.getData().getVars();
        Assert.assertNotNull(res.get("提取变量"));



        p1 = new HashMap<String, String>();
        p1.put("上下条", "下一条");

        groupVars = new ArrayList<>();
        groupVars.add(p1);

        vars = new HashMap<>();
        vars.put("上下条", "下一条");
        vars.put("新闻标题", "");
        vars.put("CURRENT_UPDATE_IN_ADVANCE_VARS", "上下条");

        params = new HashMap<>();

        params.put("chat_key", A_KEY);
        params.put("appVersion", "");
        params.put("botAccount", "");
        params.put("uid", "");
        params.put("query_text", "上下条甲");
        params.put("originText", "下一条");
        params.put("vars", vars);
        params.put("groupVars", groupVars);

        reaction = mockMvc
                .perform(post("/api/form/news/")
                        .contentType(APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(JSONObject.toJSONString(params)))
                .andDo(MockMvcResultHandlers.print());  // 控制台打印出请求体

        reaction.andExpect(MockMvcResultMatchers.status().isOk());
        mvcResult =reaction.andReturn();
//        System.out.println(mvcResult.getResponse().getContentAsString());

        response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), FormResponse.class);

        res = (Map) response.getData().getVars();
        Assert.assertNotNull(res.get("提取变量"));


    }











}
