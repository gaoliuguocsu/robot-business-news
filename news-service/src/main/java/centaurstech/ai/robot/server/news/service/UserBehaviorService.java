package centaurstech.ai.robot.server.news.service;

import centaurstech.ai.robot.server.common.result.QueryResultType;
import centaurstech.ai.robot.server.news.dao.NewsCacheRepository;
import centaurstech.ai.robot.server.news.dao.UserBehaviorRepository;
import centaurstech.ai.robot.server.news.pojo.user.UserBehavior;
import centaurstech.ai.robot.server.news.utils.DateUtils;
import com.centaurstech.utils.ChatApi;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;

import static centaurstech.ai.robot.server.common.configuration.ChatApiConfig.SERVER_SALT;

/**
 * @author yiklam
 * @description
 * @create 2021-05-06 10:39
 **/
@Slf4j
@Service
public class UserBehaviorService {

    @Autowired
    protected UserBehaviorRepository userBehaviorRepository;


    @Autowired
    private MongoTemplate mongoTemplate;


    /**
     * 对应分类权重增加
     * @param catalogName
     */
    public void increaseCatalog(String uid, String catalogName){

        UserBehavior bean = userBehaviorRepository.findOneByUid(uid);
        if (bean == null){
            bean = new UserBehavior(uid);
        }

        bean.increaseCatalog(catalogName);

        //更新如果找不到就插入 upsert()
        mongoTemplate.upsert(Query.query(Criteria.where("uid").is(uid)),
                Update.update("uid", uid).set("catalogMap", bean.getCatalogMap()).set("timeperiodMap", bean.getTimeperiodMap()).set("lastVisitTime", bean.getLastVisitTime()), UserBehavior.class);

    }


    /**
     * 对应时段权重增加
     */
    public void increasePeriod(String uid){

        Date currentTime = new Date();

        Integer period = DateUtils.getTimePeriod(currentTime);
        UserBehavior bean = userBehaviorRepository.findOneByUid(uid);
        if (bean == null){
            bean = new UserBehavior(uid);
        }

        Integer lastPeriod = DateUtils.getTimePeriod(bean.getLastVisitTime());
        if (period != 0 && lastPeriod != 0 && period != lastPeriod) {
            bean.setLastVisitTime(currentTime.getTime());
            bean.increasePeriod(period);
        }

        //更新如果找不到就插入 upsert()
        mongoTemplate.upsert(Query.query(Criteria.where("uid").is(uid)),
                Update.update("uid", uid).set("catalogMap", bean.getCatalogMap()).set("timeperiodMap", bean.getTimeperiodMap()).set("lastVisitTime", bean.getLastVisitTime()), UserBehavior.class);

    }

}
