package centaurstech.ai.robot.server.news.pojo.query;

import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import com.centaurstech.domain.FormRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class NewsQuery extends HashMap<String, String> {



    public NewsQuery(){}

    public NewsQuery(FormRequest<Map<String, String>, Map<String, String>> request){

//        this.putAll(request.getVars());
        this.put(FormVarName.CHAT_KEY, request.getChatKey());
        this.put(FormVarName.UID, request.getUid());
        this.put(FormVarName.CHANNEL_ID, request.getChannelId());
        this.put(FormVarName.QUERY_TEXT, request.getQueryText());
        this.put(FormVarName.BOT_ACCOUNT, request.getBotAccount());

        for (String key : request.getVars().keySet()){
            String value = request.getVars().get(key);
            List<String> values = Arrays.stream(value.split("\\+")).distinct().collect(Collectors.toList());
            StringBuffer keyword = new StringBuffer();
            for (String s : values){
                keyword.append(s).append("+");
            }

            keyword = keyword.deleteCharAt(keyword.length()-1);

            this.put(key, keyword.toString());

            switch (key) {
                case FormVarName.VARS_NAME:
                case FormVarName.VARS_NEWS_PROGRAM:
                case FormVarName.VARS_HOT:
                case FormVarName.VARS_EVENT:
                case FormVarName.VARS_EARLY_EVENING:

                    this.put(FormVarName.PARAM_KEYWORD, keyword.toString());
                    break;

            }

        }

    }


}
