package centaurstech.ai.robot.server.news.service;

import centaurstech.ai.robot.server.common.result.QueryResultType;
import com.centaurstech.utils.ChatApi;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static centaurstech.ai.robot.server.common.configuration.ChatApiConfig.SERVER_SALT;
import static centaurstech.ai.robot.server.common.result.QueryResultType.*;

/**
 * @author yiklam
 * @description
 * @create 2021-05-06 10:39
 **/
@Slf4j
@Service
public class ChatApiService {

    @Autowired
    private ChatApi chatApi;

    @Autowired
    private ObjectMapper objectMapper;


    public String  sendNewsList(JSONObject jsonObject) throws IOException {
        return sendJson(NEWS_LIST, jsonObject);
    }

//    public String sendNewsBean(JSONObject jsonObject) throws IOException {
//        return sendJson(NAVIGATION_DIRECTION_WITH_TRANSITS, jsonObject);
//    }


    private String sendJson(QueryResultType queryResultType, JSONObject jsonObject) throws IOException {
        String ticket = chatApi.sendJson(queryResultType.toString(), jsonObject, SERVER_SALT);
        String dataJson = log.isDebugEnabled() ? jsonObject.toString(1) : jsonObject.toString();
//        log.info("queryResultType= {}, uuid = {}, data = \n{}", queryResultType.getDesc(), ticket, dataJson);
        return ticket;
    }


}
