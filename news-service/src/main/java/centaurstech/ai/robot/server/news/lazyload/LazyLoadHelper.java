package centaurstech.ai.robot.server.news.lazyload;

import lombok.extern.slf4j.Slf4j;

import java.util.function.Supplier;

/**
 * @Classname LazyLoadHelper
 * @Description 懒加载辅助类
 * @Date 2021/2/2 15:07
 * @Created by yiklam lau
 */
@Slf4j
public class LazyLoadHelper<R> {

    private Supplier<R> supplier;

    private R r;

    private String desc;

    public LazyLoadHelper(Supplier<R> supplier, String desc) {
        this.supplier = supplier;
        this.desc = desc;
    }

    public R getData(){
        lazyLoad();
        return r;
    }

    private R lazyLoad(){
        if(r == null){
            try{
                synchronized (this){
                    if(r == null){
                        log.info("懒加载：【{}】", desc);
                        this.r = supplier.get();
                    }
                }
            }catch (Exception e){
                log.error("懒加载[{}]失败", desc);
                throw new RuntimeException("懒加载失败，请处理");
            }
        }
        return r;
    }




}
