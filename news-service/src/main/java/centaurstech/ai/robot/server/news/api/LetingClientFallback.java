package centaurstech.ai.robot.server.news.api;

import centaurstech.ai.robot.server.common.response.ApiQueryResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class LetingClientFallback implements LetingClient{


    @Override
    public ApiQueryResult newsRec(String province, String city) {
        log.info("LetingClientFallback newsRec...");
        ApiQueryResult iApiResult = new ApiQueryResult();
        iApiResult.setStatus(500);
        iApiResult.setData("");
        iApiResult.setRetcode(1);
        return iApiResult;
    }

    @Override
    public ApiQueryResult newsCatalogs() {
        log.info("LetingClientFallback newsCatalogs...");
        ApiQueryResult iApiResult = new ApiQueryResult();
        iApiResult.setStatus(500);
        iApiResult.setData("");
        iApiResult.setRetcode(1);
        return iApiResult;
    }

    @Override
    public ApiQueryResult newsChannel(String catalogId, String keyword, String startTime, String endTime, String province, String city) {
        log.info("LetingClientFallback newsChannel...");
        ApiQueryResult iApiResult = new ApiQueryResult();
        iApiResult.setStatus(500);
        iApiResult.setData("");
        iApiResult.setRetcode(1);
        return iApiResult;
    }

    @Override
    public ApiQueryResult search(String keyword, String source, String publishTime, String province, String city) {
        log.info("LetingClientFallback search...");
        ApiQueryResult iApiResult = new ApiQueryResult();
        iApiResult.setStatus(500);
        iApiResult.setData("");
        iApiResult.setRetcode(1);
        return iApiResult;
    }

    @Override
    public ApiQueryResult newsBrief(String catalogId, String province, String city, Integer type) {
        log.info("LetingClientFallback newsBrief...");
        ApiQueryResult iApiResult = new ApiQueryResult();
        iApiResult.setStatus(500);
        iApiResult.setData("");
        iApiResult.setRetcode(1);
        return iApiResult;
    }

    @Override
    public ApiQueryResult newsQuery(String query) {
        log.info("LetingClientFallback newsQuery...");
        ApiQueryResult iApiResult = new ApiQueryResult();
        iApiResult.setStatus(500);
        iApiResult.setData("");
        iApiResult.setRetcode(1);
        return iApiResult;
    }

    @Override
    public ApiQueryResult newsRegions() {
        log.info("LetingClientFallback newsRegions...");
        ApiQueryResult iApiResult = new ApiQueryResult();
        iApiResult.setStatus(500);
        iApiResult.setData("");
        iApiResult.setRetcode(1);
        return iApiResult;
    }
}
