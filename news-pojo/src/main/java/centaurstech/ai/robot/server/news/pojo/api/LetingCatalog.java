package centaurstech.ai.robot.server.news.pojo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

@Slf4j
@ToString
@Data
public class LetingCatalog {

    private String catalogId;
    private String catalogName;
    private Integer userSubscribe;


    public LetingCatalog(){}

    public LetingCatalog(JSONObject json){
        catalogId = json.getString("catalog_id");
        catalogName = json.getString("catalog_name");
        userSubscribe = json.getInteger("user_subscribe");
    }


}
