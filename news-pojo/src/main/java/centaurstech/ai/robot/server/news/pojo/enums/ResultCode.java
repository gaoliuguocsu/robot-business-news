package centaurstech.ai.robot.server.news.pojo.enums;

public enum ResultCode {

    SUCCESS(1, "SUCCESS"),
    ERROR(2, "ERROR"),

    EMPTY(3, "没有找到相关数据");

    private Integer code;
    private String msg;
    ResultCode(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
