package centaurstech.ai.robot.server.news.pojo;

import lombok.Data;
import lombok.ToString;

/**
 * @author: glg
 * @description: 新闻列表数据响应类
 * @date: 2021-06-02
 */

@ToString
@Data
public class NewsResponse {

    private Integer code;
    private String msg;
    private String service = "NEWS";
    private Object newsList;

    public NewsResponse(){}

    public NewsResponse(int code, String msg, Object data){
        this.code = code;
        this.msg = msg;
        this.newsList = data;
    }

    public NewsResponse(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

}
