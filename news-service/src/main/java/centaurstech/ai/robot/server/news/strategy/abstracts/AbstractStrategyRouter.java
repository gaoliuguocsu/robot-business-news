package centaurstech.ai.robot.server.news.strategy.abstracts;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Objects;

/**
 * 通用的“策略树“框架，通过树形结构实现分发与委托，每层通过指定的参数进行向下分发委托，直到达到最终的执行者。 
 * 该框架包含两个类：{@code StrategyHandler} 和 {@code AbstractStrategyRouter} 
 * 其中：通过实现 {@code AbstractStrategyRouter} 抽象类完成对策略的分发， 
 * 实现 {@code StrategyHandler} 接口来对策略进行实现。 
 * 像是第二层 A、B 这样的节点，既是 Root 节点的策略实现者也是策略A1、A2、B1、B2 的分发者，这样的节点只需要 
 * 同时继承 {@code StrategyHandler} 和实现 {@code AbstractStrategyRouter} 接口就可以了。 
 * 
 * <pre> 
 *           +---------+ 
 *           |  Root   |   ----------- 第 1 层策略入口 
 *           +---------+ 
 *            /       \  ------------- 根据入参 P1 进行策略分发 
 *           /         \ 
 *     +------+      +------+ 
 *     |  A   |      |  B   |  ------- 第 2 层不同策略的实现 
 *     +------+      +------+ 
 *       /  \          /  \  --------- 根据入参 P2 进行策略分发 
 *      /    \        /    \ 
 *   +---+  +---+  +---+  +---+ 
 *   |A1 |  |A2 |  |B1 |  |B2 |  ----- 第 3 层不同策略的实现 
 *   +---+  +---+  +---+  +---+ 
 * </pre> 
 * 
 * @author 
 * @date 
 * @see StrategyHandler 
 */
@Slf4j
@Component
public abstract class AbstractStrategyRouter<P, C, N, R> extends ApplicationObjectSupport implements BeanNameAware {

    /**
     * 策略映射器，根据指定的入参路由到对应的策略处理者。
     *
     * @param <P> bot提交的map
     * @param <C> 记忆变量
     */
    public interface StrategyMapper<P, C, N, R> {
        /**
         * 根据入参获取到对应的策略处理者。可通过 if-else 实现，也可通过 Map 实现。
         *
         * @param p requestParamMap
         * @param c cache
         * @return 策略处理者
         */
        StrategyHandler<P, C, N, R> get(P p, C c) throws IOException;
    }

    private StrategyMapper<P, C, N, R> strategyMapper;

    /**
     * 类初始化时注册分发策略 Mapper
     */
    @PostConstruct
    private void abstractInit() {
        strategyMapper = registerStrategyMapper();
        Objects.requireNonNull(strategyMapper, "strategyMapper cannot be null");
    }

    @Getter
    @Setter
    @SuppressWarnings("unchecked")
    private StrategyHandler<P, C, N, R> defaultStrategyHandler = StrategyHandler.DEFAULT;

    /**
     * 执行策略，框架会自动根据策略分发至下游的 Handler 进行处理
     *
     * @return 下游执行者给出的返回值
     */
    public StrategyResult<P, C, R> applyStrategy(P p, C c) throws IOException {
        final StrategyHandler<P, C, N, R> strategyHandler = strategyMapper.get(p, c);
        String simpleName = this.getClass().getSimpleName();
        log.info("——————————————————————从[{}]分发到strategyHandler: [type={},order={},handlerName={}]", simpleName, strategyHandler.getType(), strategyHandler.getOrder(), strategyHandler.getHandlerName());
        if (strategyHandler != null) {
            return strategyHandler.apply(p, c);
        }

        return defaultStrategyHandler.apply(p, c);
    }

    /**
     * 抽象方法，需要子类实现策略的分发逻辑
     *
     * @return 分发逻辑 Mapper 对象
     */
    protected abstract StrategyMapper<P, C, N, R> registerStrategyMapper();

    protected StrategyMapper<P, C, N, R> getStrategyMapper(){
        return this.strategyMapper;
    }

    /**
     *
     * @param name
     */
    @Override
    public void setBeanName(String name) {
        log.info("AbstractStrategyRouter：[{}]", this.getClass().getSimpleName());
    }
}