package centaurstech.ai.robot.server.news.strategy.pre;

import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.constants.NewsContants;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.MessageFormat;

@Slf4j
@Component
public class SwitchNewsHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.SWITCH_NEWS;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.PRE;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("切换新闻处理器...");

        if (cache == null || cache.getNewList() == null){
            return StrategyResult.toNextPre(params, cache);
        }

        StrategyResult<NewsQuery, NewsCache, CommonResult> result = null;

        //播放上一条下一条
        String value = params.get(FormVarName.VARS_PREV_AND_NEXT);
        if (StringUtils.isNotEmpty(value)){
            if (FormVarName.VALUE_PREV.equals(value)){
                //上一条
                Integer index = cache.getIndex() > 0? cache.getIndex()-1 : 0;
                LetingNews news = cache.getNewList().get(index);
                cache.setIndex(index);
                memoryService.saveCache(cache);

                String answer = MessageFormat.format(NewsContants.START_NEWS,FormVarName.VALUE_PREV);
                String newTitle = news.getTitle();
                CommonResult commonResult = new CommonResult("", "", answer, newTitle);
                result = StrategyResult.done(cache, commonResult);
                return result;

            }else if (FormVarName.VALUE_NEXT.equals(value)){
                //下一条
                Integer index = cache.getIndex() < (cache.getNewList().size()-1)? (cache.getIndex()+1) : (cache.getNewList().size()-1);
                LetingNews news = cache.getNewList().get(index);
                cache.setIndex(index);
                memoryService.saveCache(cache);

                String answer = MessageFormat.format(NewsContants.START_NEWS,FormVarName.VALUE_NEXT);
                String newTitle = news.getTitle();
                CommonResult commonResult = new CommonResult("", "", answer, newTitle);
                result = StrategyResult.done(cache, commonResult);
                return result;

            }else if (FormVarName.VALUE_GOON.equals(value)){
                //继续播放当前
                Integer index = cache.getIndex();
                LetingNews news = cache.getNewList().get(index);

                String answer = MessageFormat.format(NewsContants.START_NEWS,FormVarName.VALUE_NEXT);
                String newTitle = news.getTitle();
                CommonResult commonResult = new CommonResult("", "", answer, newTitle);
                result = StrategyResult.done(cache, commonResult);
                return result;
            }
        }

        //播放第几个  播放倒数第几个
        String orderNum = params.get(FormVarName.VARS_ORDER_NUM);
        if (StringUtils.isNotEmpty(orderNum)){
            Integer index = Integer.valueOf(orderNum);

            String reverse = params.get(FormVarName.VARS_REVERSE);

            String tag = null;
            if (StringUtils.isNotEmpty(reverse)){
                index = cache.getNewList().size() > index? (cache.getNewList().size() - index) : 0;
                tag = "倒数第" + orderNum + "条";
            }else {

                index = index < (cache.getNewList().size()) ? (index - 1) : (cache.getNewList().size() - 1);
                tag = "第" + orderNum + "条";
            }
            LetingNews news = cache.getNewList().get(index);
            cache.setIndex(index);
            memoryService.saveCache(cache);

            String answer = MessageFormat.format(NewsContants.START_NEWS, tag);
            String newTitle = news.getTitle();
            CommonResult commonResult = new CommonResult("", "", answer, newTitle);
            result = StrategyResult.done(cache, commonResult);
            return result;

        }

        //更换一个  更换一批
        String change = params.get(FormVarName.VARS_CHANGE);
        String changeUnit = params.get(FormVarName.VARS_CHANGE_UNIT);
        if (StringUtils.isNotEmpty(change)){

            Integer index = cache.getIndex() < (cache.getNewList().size()-1)? (cache.getIndex()+1) : (cache.getNewList().size()-1);
            LetingNews news = cache.getNewList().get(index);
            cache.setIndex(index);
            memoryService.saveCache(cache);

            String answer = MessageFormat.format(NewsContants.START_NEWS,FormVarName.VALUE_NEXT);
            String newTitle = news.getTitle();
            CommonResult commonResult = new CommonResult("", "", answer, newTitle);
            result = StrategyResult.done(cache, commonResult);
            return result;

        }

        return StrategyResult.toNextPre(params, cache);
    }
}
