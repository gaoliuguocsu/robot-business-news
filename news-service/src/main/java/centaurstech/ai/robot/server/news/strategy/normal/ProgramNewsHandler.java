package centaurstech.ai.robot.server.news.strategy.normal;

import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.NewsEntity;
import centaurstech.ai.robot.server.news.pojo.NewsResponse;
import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.constants.NewsContants;
import centaurstech.ai.robot.server.news.pojo.enums.ResultCode;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;

@Slf4j
@Component
public class ProgramNewsHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.NEWS_PROGRAM;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.NORMAL;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("节目新闻处理器...");

        StrategyResult<NewsQuery, NewsCache, CommonResult> result = null;

        //指定具体节目
        String province = params.get(FormVarName.PARAM_PROVINCE) == null? "" : params.get(FormVarName.PARAM_PROVINCE);
        String city = params.get(FormVarName.PARAM_CITY) == null? "" : params.get(FormVarName.PARAM_CITY);

        String keyword = params.get(FormVarName.VARS_NEWS_PROGRAM);
        if (StringUtils.isNotEmpty(keyword)){
            List<LetingNews> list = letingService.search(keyword, "", NewsContants.FOUNDED_NEWS, province, city);
            if (list != null) {
                cache.setNewsQuery(params);
                Integer index = 0;
                cache.setIndex(index);
                cache.setNewList(list);
                memoryService.saveCache(cache);

                //将数据直接返回中控
                NewsResponse respObj = new NewsResponse(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), NewsEntity.convert(list));
                String s = objectMapper.writeValueAsString(respObj);
                String ticket =  chatApiService.sendNewsList(new JSONObject(s));

                String newsStr = generateNews(list);
//                String newsStr = list.get(index).getTitle();
                CommonResult commonResult = new CommonResult( NewsContants.FOUNDED_NEWS, newsStr, ticket);
                result = StrategyResult.done(cache, commonResult);
                return result;
            }else{
                cache.setNewsQuery(params);
                memoryService.saveCache(cache);

                //将数据直接返回中控
//                NewsResponse respObj = new NewsResponse(ResultCode.EMPTY.getCode(), ResultCode.EMPTY.getMsg());
//                String s = objectMapper.writeValueAsString(respObj);
//                String ticket =  chatApiService.sendNewsList(new JSONObject(s));

                String answer = MessageFormat.format(NewsContants.NOT_FOUND_NEWS, keyword);
                CommonResult commonResult = new CommonResult("", "", answer, "");
                result = StrategyResult.done(cache, commonResult);
                return result;
            }
        }


        result = StrategyResult.toNextPre(params, cache);
        return result;
    }
}
