package centaurstech.ai.robot.server.news.strategy.normal;

import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.NewsEntity;
import centaurstech.ai.robot.server.news.pojo.NewsResponse;
import centaurstech.ai.robot.server.news.pojo.api.LetingCatalog;
import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.constants.NewsContants;
import centaurstech.ai.robot.server.news.pojo.enums.ResultCode;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;

@Slf4j
@Component
public class TimeNewsHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.NEWS_TIME;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.NORMAL;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("时间新闻处理器...");

        StrategyResult<NewsQuery, NewsCache, CommonResult> result = null;

        String publishTime = params.get(FormVarName.PARAM_TIME_DATE) == null ? "" : params.get(FormVarName.PARAM_TIME_DATE);
        String province = params.get(FormVarName.PARAM_PROVINCE) == null? "" : params.get(FormVarName.PARAM_PROVINCE);
        String city = params.get(FormVarName.PARAM_CITY) == null? "" : params.get(FormVarName.PARAM_CITY);
        String keyword = params.get(FormVarName.PARAM_KEYWORD) == null? "" : params.get(FormVarName.PARAM_KEYWORD);
        keyword = "".equals(keyword)? (params.get(FormVarName.VARS_AREA)==null? "" : params.get(FormVarName.VARS_AREA)) : keyword;

        String timeType = params.get(FormVarName.PARAM_TIME_TYPE) == null? "1" : params.get(FormVarName.PARAM_TIME_TYPE);

        List<LetingNews> list = null;
        if ("2".equals(timeType) && StringUtils.isEmpty(keyword)) {

            String startTime = params.get(FormVarName.PARAM_TIME_START)==null? "" : params.get(FormVarName.PARAM_TIME_START);
            String endTime = params.get(FormVarName.PARAM_TIME_END)==null? "" : params.get(FormVarName.PARAM_TIME_END);

            String channelName = "社会";
            LetingCatalog catalog = letingService.getCatalogByName(channelName);
            list = letingService.newsChannel(catalog.getCatalogId(), "", startTime, endTime, province, city);

        }else {
            list = letingService.search(keyword, "", publishTime, province, city);

        }

        if (list != null) {
            cache.setNewsQuery(params);
            Integer index = 0;
            cache.setIndex(index);
            cache.setNewList(list);
            memoryService.saveCache(cache);

            //将数据直接返回中控
            NewsResponse respObj = new NewsResponse(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), NewsEntity.convert(list));
            String s = objectMapper.writeValueAsString(respObj);
            String ticket = chatApiService.sendNewsList(new JSONObject(s));

            String newsStr = generateNews(list);
//            String newsStr = list.get(index).getTitle();
            CommonResult commonResult = new CommonResult(NewsContants.FOUNDED_NEWS, newsStr, ticket);
            result = StrategyResult.done(cache, commonResult);
            return result;
        }

//            cache.setNewsQuery(params);
//            memoryService.saveCache(cache);

        CommonResult commonResult = new CommonResult("", "", NewsContants.NOT_FOUND_NEWS, "");
        result = StrategyResult.done(cache, commonResult);
        return result;


    }
}
