package centaurstech.ai.robot.server.news.pojo.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@ToString
@Data
public class UserBehavior {

    /**
     * 用户标识
     */
    private String uid;

    /**
     * 用户类别权重
     */
    private Map<String, Integer> catalogMap;


    /**
     * 用户时段权重
     */
    private Map<Integer, Integer> timeperiodMap;


    /**
     * 上次访问时间
     */
    private Long lastVisitTime = 0L;



    public UserBehavior(String uid){
        this.uid  = uid;
        catalogMap =  new HashMap<String, Integer>();
        timeperiodMap =  new HashMap<Integer, Integer>();
    }

    /**
     * 对应分类权重增加
     * @param catalogName
     */
    public void increaseCatalog(String catalogName){

        if (catalogMap == null){
            catalogMap =  new HashMap<String, Integer>();
        }

        Integer weight = catalogMap.get(catalogName);
        if (weight == null){
            weight = 0;
        }

        catalogMap.put(catalogName, ++weight);
    }


    /**
     * 对应时段权重增加
     * @param period
     */
    public void increasePeriod(Integer period){
        if (timeperiodMap == null){
            timeperiodMap =  new HashMap<Integer, Integer>();
        }

        Integer weight = timeperiodMap.get(period);
        if (weight == null){
            weight = 0;
        }

        timeperiodMap.put(period, ++weight);
    }


}
