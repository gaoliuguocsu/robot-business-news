#!/bin/bash
set -e				# 遇到错误就退出

echo "Runing deploy script ... User:${USER}"

username="root"
host="hw-gz35.heyqiwu.cn"
port="22222"

pemFilePath="./script/ssh-key/robot-server.pem"
restartScriptPath="./script/restart-xw-uat.sh"

#jarPath=$(find ./build/libs/ -name "*.jar")
jarPath=$(find ./Navigation-Service/build/ -name "*.jar")
staticFiles="./data"


appFolder="robot-business-navigation/"

echo "Deploying to ... ${appFolder}"

# chmod pem
chmod 400 ${pemFilePath}

# prepare ssh command
sshParams="-o StrictHostKeyChecking=no -i ${pemFilePath}"

# ssh to create folder if not exsit
ssh -p ${port} ${sshParams} ${username}@${host} "mkdir -p /root/${appFolder};pwd;ls"

# debug log scp command
echo "scp -P ${port} ${sshParams} ${jarPath} ${username}@${host}:~/${appFolder}"
# scp jar file to hosts
scp -P ${port} ${sshParams} ${jarPath} ${username}@${host}:~/${appFolder}
# scp data folder
scp -r -P ${port} ${sshParams} ${staticFiles} ${username}@${host}:~/${appFolder}

# ssh to thoses hosts and run 'restart.sh'
echo "run script ${restartScriptPath}"
ssh -p ${port} -t ${sshParams} ${username}@${host} 'bash -s' -- < ${restartScriptPath} ${appFolder}
