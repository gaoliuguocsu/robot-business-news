package centaurstech.ai.robot.server.news.strategy.base;

import centaurstech.ai.robot.server.news.dao.NewsCacheRepository;
import centaurstech.ai.robot.server.news.service.ChatApiService;
import centaurstech.ai.robot.server.news.service.LetingService;
import centaurstech.ai.robot.server.news.service.MemoryService;
import centaurstech.ai.robot.server.news.service.UserBehaviorService;
import com.centaurstech.redis.service.CacheService;
import com.centaurstech.redis.service.v2.CacheServiceV2;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author yiklam
 * @description
 * @create 2021-05-06 14:14
 **/
public abstract class BaseServiceHolder {


    protected CacheService cacheService;

    protected LetingService letingService;

    protected ChatApiService chatApiService;

    protected MemoryService memoryService;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected UserBehaviorService userBehaviorService;


    @Autowired
    public final void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Autowired
    public final void setLetingService(LetingService letingService) {
        this.letingService = letingService;
    }

    @Autowired
    public void setChatApiService(ChatApiService chatApiService) {
        this.chatApiService = chatApiService;
    }

    @Autowired
    public void setMemoryService(MemoryService memoryService) {
        this.memoryService = memoryService;
    }
}
