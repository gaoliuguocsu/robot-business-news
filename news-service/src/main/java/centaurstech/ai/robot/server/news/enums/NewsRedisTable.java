package centaurstech.ai.robot.server.news.enums;

import com.centaurstech.redis.interfaces.RedisKey;

public enum NewsRedisTable implements RedisKey {


    NEWS_CACHE("NEWS_CACHE"),
    USER_BEHAVIOR_CACHE("USER_BEHAVIOR_CACHE");

    private String description;
    NewsRedisTable(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 返回description
     *
     * @return
     */
    @Override
    public String getKey() {
        return this.name();
    }
}
