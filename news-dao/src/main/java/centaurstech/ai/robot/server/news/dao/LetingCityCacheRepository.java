package centaurstech.ai.robot.server.news.dao;

import centaurstech.ai.robot.server.news.pojo.api.LetingCity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LetingCityCacheRepository extends MongoRepository<LetingCity, String> {

    LetingCity findOneByCityLike(String name);

}
