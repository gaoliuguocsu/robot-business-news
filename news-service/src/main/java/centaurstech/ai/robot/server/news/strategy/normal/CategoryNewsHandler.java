package centaurstech.ai.robot.server.news.strategy.normal;

import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.NewsEntity;
import centaurstech.ai.robot.server.news.pojo.NewsResponse;
import centaurstech.ai.robot.server.news.pojo.api.LetingCatalog;
import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.constants.NewsContants;
import centaurstech.ai.robot.server.news.pojo.enums.ResultCode;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;

@Slf4j
@Component
public class CategoryNewsHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.NEWS_CATEGORY;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.NORMAL;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("类别新闻处理器...");

        StrategyResult<NewsQuery, NewsCache, CommonResult> result = null;


        String categoryName = params.get(FormVarName.VARS_NEWS_CATEGORY);
        if (StringUtils.isNotEmpty(categoryName)){

            String province = params.get(FormVarName.PARAM_PROVINCE) == null? "" : params.get(FormVarName.PARAM_PROVINCE);
            String city = params.get(FormVarName.PARAM_CITY) == null? "" : params.get(FormVarName.PARAM_CITY);

            List<LetingNews> list = null;

            StringBuffer catalogIds = new StringBuffer();
            String[] cates = categoryName.split("\\+");

            String negate = params.get(FormVarName.VARS_NEGATE);
            if (StringUtils.isNotEmpty(negate)){
                // 不想听某类别...
                List<LetingCatalog> catalogs = letingService.excludeCatalog(cates);
                for (LetingCatalog catalog : catalogs){
                    catalogIds.append(catalog.getCatalogId()).append(",");
                }

            }else {
                for (String c : cates) {
                    LetingCatalog catalog = letingService.getCatalogByName(c);
                    if (catalog != null) {
                        catalogIds.append(catalog.getCatalogId()).append(",");
                    }
                }
            }

            if (catalogIds.length() > 0){
                //找到对应 类别 直接使用类别接口

                catalogIds = catalogIds.deleteCharAt(catalogIds.length() - 1);

                String startTime = params.get(FormVarName.PARAM_TIME_START)==null? "" : params.get(FormVarName.PARAM_TIME_START);
                String endTime = params.get(FormVarName.PARAM_TIME_END)==null? "" : params.get(FormVarName.PARAM_TIME_END);
                String keyword = "";

                if (StringUtils.isEmpty(province) && StringUtils.isEmpty(city) && StringUtils.isEmpty(keyword)){
                    String area = params.get(FormVarName.VARS_AREA);
                    if (StringUtils.isNotEmpty(area)){
                        keyword = area;
                    }
                }

                list = letingService.newsChannel(catalogIds.toString(), keyword, startTime, endTime, province, city);

            }else{
                //没有找到对应类别, 则使用搜索接口

                String time = params.get(FormVarName.PARAM_TIME_DATE) == null ? "" : params.get(FormVarName.PARAM_TIME_DATE);
                String source = "";
                list = letingService.search(categoryName, source, time, province, city);
            }

            if (list != null) {

                //保存缓存
                cache.setNewsQuery(params);
                Integer index = 0;
                cache.setIndex(index);
                cache.setNewList(list);
                memoryService.saveCache(cache);

                //将数据直接返回中控
                NewsResponse respObj = new NewsResponse(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), NewsEntity.convert(list));
                String s = objectMapper.writeValueAsString(respObj);
                String ticket =  chatApiService.sendNewsList(new JSONObject(s));

                String newsStr = generateNews(list);
//                String newsStr = list.get(index).getTitle();
                CommonResult commonResult = new CommonResult(NewsContants.FOUNDED_NEWS, newsStr, ticket);
                result = StrategyResult.done(cache, commonResult);
                return result;
            }else{

                //保存缓存
                cache.setNewsQuery(params);
                memoryService.saveCache(cache);

//                //将数据直接返回中控
//                NewsResponse respObj = new NewsResponse(ResultCode.EMPTY.getCode(), ResultCode.EMPTY.getMsg());
//                String s = objectMapper.writeValueAsString(respObj);
//                String ticket =  chatApiService.sendNewsList(new JSONObject(s));

                String answer = MessageFormat.format(NewsContants.NOT_FOUND_NEWS, categoryName);
                CommonResult commonResult = new CommonResult(answer, "", "");
                result = StrategyResult.done(cache, commonResult);
                return result;
            }

        }


        result = StrategyResult.toNextPre(params, cache);
        return result;
    }
}
