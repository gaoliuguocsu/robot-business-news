package centaurstech.ai.robot.server.news.pojo.cache;

import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Slf4j
@ToString
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewsCache {

    private String chatKey;

    private String uid;

    private NewsQuery newsQuery;

    private Integer index;

    private List<LetingNews> newList;

    public NewsCache(){}

    public NewsCache(String chatKey){
        this.chatKey = chatKey;
    }

}
