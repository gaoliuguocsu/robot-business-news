package centaurstech.ai.robot.server.news.strategy.pre;

import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import centaurstech.ai.robot.server.news.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

@Slf4j
@Component
public class TimeConvertHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.NEWS_TIME_CONVERT;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.PRE;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("时间转换处理...");

        StrategyResult<NewsQuery, NewsCache, CommonResult> result = null;

        //昨天 前天等时间表述转换成具体的时间值
        String time1 = params.get(FormVarName.VARS_TIME_1);
        String time2 = params.get(FormVarName.VARS_TIME_2);
        String time3 = params.get(FormVarName.VARS_TIME_FRONT_BACK);

        String time = time1==null? (time2==null? time3 : time2) : time1;
        if (time == null){
            return StrategyResult.toNextPre(params, cache);
        }

        Integer timeType = DateUtils.getTimeType(time);
        params.put(FormVarName.PARAM_TIME_TYPE, String.valueOf(timeType));

        Date startTime = DateUtils.getTimeBegin(time);
        if (startTime != null) {
            params.put(FormVarName.PARAM_TIME_DATE, DateUtils.getDateFormat(startTime, DateUtils.FORMAT_1));
            params.put(FormVarName.PARAM_TIME_START, DateUtils.getDateFormat(startTime, DateUtils.FORMAT_2));
        }

        Date endTime = DateUtils.getTimeEnd(time);
        if (endTime != null){
            params.put(FormVarName.PARAM_TIME_END, DateUtils.getDateFormat(endTime, DateUtils.FORMAT_2));
        }

        return StrategyResult.toNextPre(params, cache);
    }
}
