package centaurstech.ai.robot.server.news.pojo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@ToString
@Data
public class LetingCity {

    private String province;
    private String city;

    public LetingCity(){}

    public LetingCity(String province, String city){
        this.province = province;
        this.city = city;
    }

}
