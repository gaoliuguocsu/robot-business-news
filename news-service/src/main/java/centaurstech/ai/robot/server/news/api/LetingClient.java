package centaurstech.ai.robot.server.news.api;

import centaurstech.ai.robot.server.common.response.ApiQueryResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(name="letingClient", url = "${feign-news-api.url}", fallback = LetingClientFallback.class)
public interface LetingClient {

    /**
     * 推荐新闻
     * @param province
     * @param city
     * @return
     */
    @GetMapping("/api/v1/newsRec")
    ApiQueryResult newsRec(@RequestParam("province") String province, @RequestParam("city") String city);


    /**
     * 频道列表
     * @return
     */
    @GetMapping("/api/v1/newsCatalogs")
    ApiQueryResult newsCatalogs();


    /**
     * 频道新闻
     * @param catalogId    频道，支持多频道(or关系)，频道之间用英文","隔开
     * @param keyword      支持标题/标签/来源搜索
     * @param startTime     开始时间  yyyy-MM-dd hh:mm:ss
     * @param endTime       结束时间  yyyy-MM-dd hh:mm:ss
     * @param province
     * @param city
     * @return
     */
    @GetMapping("/api/v1/newsChannel")
    ApiQueryResult newsChannel(@RequestParam("catalogId") String catalogId, @RequestParam("keyword") String keyword, @RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime, @RequestParam("province") String province, @RequestParam("city") String city);


    /**
     * 搜索
     * @param keyword
     * @param source
     * @param publishTime
     * @param province
     * @param city
     * @return
     */
    @GetMapping("/api/v1/newsSearch")
    ApiQueryResult search(@RequestParam("keyword") String keyword, @RequestParam("source") String source, @RequestParam("publishTime") String publishTime, @RequestParam("province") String province, @RequestParam("city") String city);


    /**
     * 早晚报
     * @param catalogId  catalog_id目前只支持：社会(综合类新闻），科技，财经，娱乐，汽车对应的频道id，默认返回社会(综合类)新闻
     * @param province
     * @param city
     * @param type       1.早报(默认)，2.晚报
     * @return
     */
    @GetMapping("/api/v1/newsBrief")
    ApiQueryResult newsBrief(@RequestParam("catalogId") String catalogId, @RequestParam("province") String province, @RequestParam("city") String city, @RequestParam("type") Integer type);


    /**
     * query
     * @param query  查询关键词
     * @return
     */
    @GetMapping("/api/v1/newsQuery")
    ApiQueryResult newsQuery(@RequestParam("query") String query);


    /**
     * 省市区
     * @return
     */
    @GetMapping("/api/v1/newsRegions")
    ApiQueryResult newsRegions();

}
