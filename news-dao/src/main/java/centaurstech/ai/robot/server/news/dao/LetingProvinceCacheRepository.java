package centaurstech.ai.robot.server.news.dao;

import centaurstech.ai.robot.server.news.pojo.api.LetingProvince;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LetingProvinceCacheRepository extends MongoRepository<LetingProvince, String> {

    LetingProvince findOneByProvinceLike(String name);

}
