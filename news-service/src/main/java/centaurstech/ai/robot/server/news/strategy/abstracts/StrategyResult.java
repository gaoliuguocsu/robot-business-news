package centaurstech.ai.robot.server.news.strategy.abstracts;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@Slf4j
public class StrategyResult<P,C,R> {

    /**
     * 是否结束处理
     */
    private boolean done = false;
    private P params;
    private C cache;
    /**
     * 返回结果
     */
    private R commonResult;


    public StrategyResult(boolean done, P params, C cache, R commonResult) {
        this.done = done;
        this.params = params;
        this.cache = cache;
        this.commonResult = commonResult;
    }

    public static<P,C,R> StrategyResult<P,C,R> done(P params, C cache, R commonResult){
        return new StrategyResult<>(true, params, cache, commonResult);
    };

    public static<P,C,R> StrategyResult<P,C,R> done(C cache, R commonResult){
        return new StrategyResult<>(true,null, cache, commonResult);
    };


    public static<P,C,R> StrategyResult<P,C,R> finish(P params, C cache){
        return new StrategyResult<>(true,  params, cache, null);
    };

    public static<P,C,R> StrategyResult<P,C,R> toNextPre(P params, C cache){
        return new StrategyResult<>(false,  params, cache, null);
    };


}
