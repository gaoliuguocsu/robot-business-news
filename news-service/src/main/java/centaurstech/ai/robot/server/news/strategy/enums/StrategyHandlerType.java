package centaurstech.ai.robot.server.news.strategy.enums;

public enum StrategyHandlerType {

    /**
     * 根处理器，入口
     */
    ROOT("根类型"),
    /**
     * 条件判断在handler里面
     */
    PRE("前置遍历处理"),
    /**
     * 条件判断不在handler里面
     */
    NORMAL("普通的"),
    /**
     * 该类型的处理器返回result为null：处于最后，必须执行，无需判断是否需要
     */
    LAST("后置遍历处理")
    ;

    StrategyHandlerType(String desc) {
        this.desc = desc;
    }

    private String desc;
}