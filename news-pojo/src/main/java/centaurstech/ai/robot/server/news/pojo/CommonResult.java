package centaurstech.ai.robot.server.news.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonResult {

    @JsonProperty(value= "请求表单")
    private String form;

    @JsonProperty(value= "响应结果")
    private String result;

    @JsonProperty(value= "回复话术")
    private String answer;

    @JsonProperty(value= "提取变量")
    private List<Map<String, String>> params;

    @JsonProperty(value= "新闻标题")
    private String newsTitle;

    @JsonProperty(value= "数据令牌")
    private String newsTicket;

    public CommonResult(){}

    public CommonResult(String form, String result, String answer, String newsTitle){
        this.form = form;
        this.result = result;
        this.answer = answer;
        this.newsTitle = newsTitle;
    }


    public CommonResult(String answer, String newsTitle, String newsTicket){
        this.answer = answer;
        this.newsTitle = newsTitle;
        if (!StringUtils.isEmpty(newsTicket)) {
            this.newsTicket = "嬲" +newsTicket;
        }
    }

}
