package centaurstech.ai.robot.server.news;

import com.centaurstech.redis.annotation.EnableCentaursRedis;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
/**
 * Created by plugin RobotInitPlugin
 * @see [https://gitlab.com/yiwangcentaurs/javaback/robot-init-plugin](https://gitlab.com/yiwangcentaurs/javaback/robot-init-plugin)
 */
@SpringBootApplication(scanBasePackages = {"centaurstech.ai.*"})
@EnableScheduling
@EnableAsync
@EnableFeignClients
@EnableMongoRepositories(basePackages = "centaurstech.ai.robot.server.news.dao")
@EnableCircuitBreaker
@EnableConfigurationProperties
@EnableCentaursRedis
@EnableAspectJAutoProxy(exposeProxy = true, proxyTargetClass = true)
public class NewsApp {
    public static void main(String[] args) {
        SpringApplication.run(NewsApp.class, args);
    }
}