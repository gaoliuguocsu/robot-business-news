package centaurstech.ai.robot.server.news.dao;

import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewsCacheRepository extends MongoRepository<NewsCache, String> {



}
