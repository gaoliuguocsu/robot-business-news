package centaurstech.ai.robot.server.news.pojo.constants;

public class NewsContants {

    /**
     * 资讯话术
     */
    public static String CLOSE_NEWS = "资讯已关闭";
    public static String LIKE_NEWS = "已点赞";
    public static String PAUSE_NEWS = "已暂停";
    public static String SHARE_NEWS = "已分享";
    public static String SETTING_NEWS = "已设置";
    public static String DELETE_NEWS = "已删除";
    public static String UPDATE_NEWS = "已修改";
    public static String EMPTY_LIST_NEWS = "列表为空";
    public static String FOUNDED_NEWS = "为你找到以下新闻";
    public static String FOUNDED_LIST_NEWS = "新闻列表";
    public static String RECOMMEND_NEWS = "为你推荐以下新闻";
    public static String NOT_FOUND_NEWS = "未找到{0}相关的新闻";
    public static String START_NEWS = "正在为你播放{0}";

    public static String EARLY_NEWS = "早报";
    public static String EVENING_NEWS = "晚报";

    public static String FUNCTION_LIST = "列表";

    public static String CHANGE_ONE = "个";
    public static String CHANGE_BATCH = "批";

}
