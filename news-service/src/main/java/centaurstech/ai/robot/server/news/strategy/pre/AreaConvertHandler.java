package centaurstech.ai.robot.server.news.strategy.pre;

import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.api.LetingCity;
import centaurstech.ai.robot.server.news.pojo.api.LetingProvince;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class AreaConvertHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.NEWS_AREA;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.PRE;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("区域转换处理...");

        // 区域转换成标准的省市
        String province = "", city = "";
        String area = params.get(FormVarName.VARS_AREA)==null? "" : params.get(FormVarName.VARS_AREA);
        if (StringUtils.isNotEmpty(area)){

            LetingProvince provinceBean = letingService.getProvinceByName(area);
            if (provinceBean != null){
                province = provinceBean.getProvince();
                params.put(FormVarName.PARAM_PROVINCE, province);
                params.put(FormVarName.PARAM_CITY, city);
                return StrategyResult.toNextPre(params, cache);
            }

            LetingCity cityBean = letingService.getCityByName(area);
            if (cityBean != null){
                province = cityBean.getProvince();
                city = cityBean.getCity();
                params.put(FormVarName.PARAM_PROVINCE, province);
                params.put(FormVarName.PARAM_CITY, city);
                return StrategyResult.toNextPre(params, cache);
            }

            params.put(FormVarName.PARAM_AREA, area);
            return StrategyResult.toNextPre(params, cache);
        }

        return StrategyResult.toNextPre(params, cache);
    }

}
