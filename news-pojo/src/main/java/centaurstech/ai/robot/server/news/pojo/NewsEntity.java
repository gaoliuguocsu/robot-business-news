package centaurstech.ai.robot.server.news.pojo;

import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import lombok.Data;
import lombok.ToString;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: glg
 * @description: 资讯数据实体
 * @date:
 */

@ToString
@Data
public class NewsEntity {

    /**
     *
     */
    private String id;
    /**
     * 新闻标题
     */
    private String title;

    /**
     * 发表时间
     */
    private String publishTime;

    /**
     * 简介
     */
    private String summary;

    /**
     * 内容
     */
    private String content;

    /**
     * 热度
     */
    private Integer hotValue;

    /**
     * 来源
     */
    private String channel;
    /**
     * 新闻类别
     */
    private String category;

    /**
     * 标签
     */
    private String tag;

    /**
     * 音频地址
     */
    private String audio;

    /**
     * 时长
     */
    private String hms;

    /**
     *
     */
    private String image;

    public NewsEntity(){}

    public NewsEntity(LetingNews data){
        this.setId(data.getSid());
        this.setTitle(data.getTitle());
        String publishTime = getDateFormat(new Date(data.getPubTime() * 1000), "HH:mm:ss");
        this.setPublishTime(publishTime);
        this.setCategory(data.getCatalogName());
        this.setChannel(data.getSource());
        this.setAudio(data.getAudio());
        this.setHotValue(data.getHot());
        this.setTag(data.getTags());
        this.setContent(data.getContent());
        this.setHms(data.getHms());
        this.setSummary(data.getSummary());
        this.setImage(data.getImage());
    }

    public String getDateFormat(Date date, String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static List<NewsEntity> convert(List<LetingNews> list){
        if (list == null || list.size() == 0){
            return null;
        }
        List<NewsEntity> result = new ArrayList<>();
        for (LetingNews bean : list){
            result.add(new NewsEntity(bean));
        }

        return result;
    }

}
