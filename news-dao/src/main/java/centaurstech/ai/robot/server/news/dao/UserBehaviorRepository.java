package centaurstech.ai.robot.server.news.dao;

import centaurstech.ai.robot.server.news.pojo.user.UserBehavior;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserBehaviorRepository extends MongoRepository<UserBehavior, String> {

    UserBehavior findOneByUid(String uid);

}
