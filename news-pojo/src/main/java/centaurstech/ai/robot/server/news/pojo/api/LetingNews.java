package centaurstech.ai.robot.server.news.pojo.api;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class LetingNews {

    private String sid;

    private String catalogId;

    private String catalogName;

    private Integer duration;

    private String image;

    private String source;

    private String sourceIcon;

    private Integer hot;

    private String tags;

    private String title;

    private String summary;

    private String content;

    private Long pubTime;

    private Long updatedAt;

    private String humanTime;

    private String hms;

    private String audio;


    public LetingNews(){}

    public LetingNews(JSONObject json){
        sid = json.getString("sid");
        catalogId = json.getString("catalog_id");
        catalogName = json.getString("catalog_name");
        duration = json.getInteger("duration");
        image = json.getString("image");
        source = json.getString("source");
        sourceIcon = json.getString("source_icon");
        hot = json.getInteger("hot");
        tags = json.getString("tags");
        title = json.getString("title");
        summary = json.getString("summary");
        content = json.getString("content");
        pubTime = json.getLong("pub_time");
        updatedAt = json.getLong("updated_at");
        humanTime = json.getString("human_time");
        hms = json.getString("hms");
        audio = json.getString("audio");
    }

}
