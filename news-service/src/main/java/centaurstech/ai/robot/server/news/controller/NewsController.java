package centaurstech.ai.robot.server.news.controller;

import centaurstech.ai.robot.server.news.dao.NewsCacheRepository;
import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.enums.ResultCode;
import centaurstech.ai.robot.server.news.service.NewsService;
import com.centaurstech.domain.FormRequest;
import com.centaurstech.domain.FormResponse;
import com.centaurstech.redis.service.CacheService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;


@RestController
@Slf4j
public class NewsController {

    public static Long BASE_BUSINESS_DATA_CLEAR_INTERVAL = 5 * 60 * 1000L;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private NewsService newsService;


    @RequestMapping(value = "/api/form/news", produces={"application/json;charset=UTF-8"}, method = {RequestMethod.POST})
    public FormResponse process(@RequestBody FormRequest<Map<String, String>, Map<String, String>> form) throws IOException {

        String reqJson = objectMapper.writeValueAsString(form);

        log.info("request: {}", reqJson);

        FormResponse.FormData<CommonResult, Map<String, String>> data = null;
        try {
            data = newsService.execute(form);
        } catch (Exception e) {
            e.printStackTrace();
        }

        log.info("response: {}", data);
        return new FormResponse(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), data);
    }

}
