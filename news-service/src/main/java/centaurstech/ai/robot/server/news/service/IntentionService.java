package centaurstech.ai.robot.server.news.service;

import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import com.centaurstech.domain.FormRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author yiklam
 * @description 意图帮助类
 * @create 2021-04-28 15:33
 **/
@Slf4j
@Component
public class IntentionService {


    /**
     * 区域新闻意图
     * @param query
     * @return
     */
    public boolean hasAreaIntention(Map query){
        return query.containsKey(FormVarName.VARS_AREA);
    }

    /**
     * 人名新闻意图
     * @param query
     * @return
     */
    public boolean hasNameIntention(Map query){
        return query.containsKey(FormVarName.VARS_NAME);
    }

    /**
     * 类别新闻意图
     * @param query
     * @return
     */
    public boolean hasCategoryIntention(Map query){
        return query.containsKey(FormVarName.VARS_NEWS_CATEGORY);
    }

    /**
     * 时间新闻意图
     * @param query
     * @return
     */
    public boolean hasTimeIntention(Map query){
        return query.containsKey(FormVarName.VARS_TIME_1) || query.containsKey(FormVarName.VARS_TIME_2) || query.containsKey(FormVarName.VARS_TIME_FRONT_BACK);
    }


    /**
     * 节目新闻意图
     * @param query
     * @return
     */
    public boolean hasProgramIntention(Map query){
        return query.containsKey(FormVarName.VARS_NEWS_PROGRAM);
    }

    /**
     * 推荐新闻意图
     * @param query
     * @return
     */
    public boolean hasRecommendIntention(Map query){
        return query.containsKey(FormVarName.VARS_NEWS);
    }

    /**
     * 事件意图
     * @param query
     * @return
     */
    public boolean hasEventIntention(Map query){
        return query.containsKey(FormVarName.VARS_EVENT);
    }



    /**
     * 早晚报意图
     * @param query
     * @return
     */
    public boolean hasEarlyEveningIntention(Map query){
        return query.containsKey(FormVarName.VARS_EARLY_EVENING);
    }

}