package centaurstech.ai.robot.server.news.utils;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {


    public static Integer getTimePeriod(Long time){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        return getTimePeriod(cal.getTime());
    }

    public static Integer getTimePeriod(Date date){
        Integer hour = getHours(date);
        if (hour >= 6  && hour < 11){         //6:00-10:59
            return 1;
        }else if (hour >= 11  && hour < 13){  //11:00-12:59
            return 2;
        }else if (hour >= 13  && hour < 15){  //13:00-14:59
            return 3;
        }else if (hour >= 15  && hour < 20){  //15:00-19:59
            return 4;
        }else if (hour >= 20  && hour < 22){  //20:00-21:59
            return 5;
        }else if (hour >= 22  && hour <= 23){  //20:00-23:59
            return 6;
        }

        return 0;
    }


    public static Integer getTimeType(String time){
        Integer type = 1;
        switch (time){
            case "今天":
            case "今日":
            case "昨天":
            case "昨日":
            case "前天":
            case "前日":
            case "大前天":
                type = 1;
                break;
            case "上周":
            case "上礼拜":
            case "上个礼拜":
            case "上月":
            case "上个月":
            case "去年":
            case "前年":
            case "大前年":
                type = 2;
                break;
        }

        return type;
    }

    /**
     * 获取得指定时间的开始
     * @param time
     * @return
     */
    public static Date getTimeBegin(String time){
        Date date = null;
        switch (time){
            case "今天":
            case "今日":
                date = getDayBegin();
                break;
            case "昨天":
            case "昨日":
                date = getBeginDayOfYesterday();
                break;
            case "前天":
            case "前日":
                date = getFrontDay(getDayBegin(), 2);
                break;
            case "大前天":
                date = getFrontDay(getDayBegin(), 3);
                break;
            case "上周":
            case "上礼拜":
            case "上个礼拜":
                date = getBeginDayOfLastWeek();
                break;
            case "上月":
            case "上个月":
                date = getBeginDayOfLastMonth();
                break;
            case "去年":
                date = getBeginDayOfNYearBefore(1);
                break;
            case "前年":
                date = getBeginDayOfNYearBefore(2);
                break;
            case "大前年":
                date = getBeginDayOfNYearBefore(3);
                break;
        }

//        if (date != null){
//            return getDateFormat(date);
//        }

        return date;
    }


    public static Date getTimeEnd(String time){
        Date date = null;
        switch (time){
            case "今天":
            case "今日":
                date = getDayEnd();
                break;
            case "昨天":
            case "昨日":
                date = getEndDayOfYesterDay();
                break;
            case "前天":
            case "前日":
                date = getFrontDay(getDayEnd(), 2);
                break;
            case "大前天":
                date = getFrontDay(getDayEnd(), 3);
                break;
            case "上周":
            case "上礼拜":
            case "上个礼拜":
                date = getEndDayOfLastWeek();
                break;
            case "上月":
            case "上个月":
                date = getEndDayOfLastMonth();
                break;
            case "去年":
                date = getEndDayOfNYearBefore(1);
                break;
            case "前年":
                date = getEndDayOfNYearBefore(2);
                break;
            case "大前年":
                date = getEndDayOfNYearBefore(3);
                break;
        }

//        if (date != null){
//            return getDateFormat(date);
//        }

        return date;
    }

    //获取当天的开始时间
    public static java.util.Date getDayBegin() {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    //获取当天的结束时间
    public static java.util.Date getDayEnd() {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    //获取昨天的开始时间
    public static Date getBeginDayOfYesterday() {
        Calendar cal = new GregorianCalendar();
        cal.setTime(getDayBegin());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return cal.getTime();
    }

    //获取昨天的结束时间
    public static Date getEndDayOfYesterDay() {
        Calendar cal = new GregorianCalendar();
        cal.setTime(getDayEnd());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return cal.getTime();
    }

    //获取明天的开始时间
    public static Date getBeginDayOfTomorrow() {
        Calendar cal = new GregorianCalendar();
        cal.setTime(getDayBegin());
        cal.add(Calendar.DAY_OF_MONTH, 1);

        return cal.getTime();
    }

    //获取明天的结束时间
    public static Date getEndDayOfTomorrow() {
        Calendar cal = new GregorianCalendar();
        cal.setTime(getDayEnd());
        cal.add(Calendar.DAY_OF_MONTH, 1);
        return cal.getTime();
    }

    //获取本周的开始时间
    @SuppressWarnings("unused")
    public static Date getBeginDayOfWeek() {
        Date date = new Date();
        if (date == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayofweek = cal.get(Calendar.DAY_OF_WEEK);
        if (dayofweek == 1) {
            dayofweek += 7;
        }
        cal.add(Calendar.DATE, 2 - dayofweek);
        return getDayStartTime(cal.getTime());
    }

    //获取本周的结束时间
    public static Date getEndDayOfWeek() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getBeginDayOfWeek());
        cal.add(Calendar.DAY_OF_WEEK, 6);
        Date weekEndSta = cal.getTime();
        return getDayEndTime(weekEndSta);
    }

    //获取上周的开始时间
    @SuppressWarnings("unused")
    public static Date getBeginDayOfLastWeek() {
        Date date = new Date();
        if (date == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayofweek = cal.get(Calendar.DAY_OF_WEEK);
        if (dayofweek == 1) {
            dayofweek += 7;
        }
        cal.add(Calendar.DATE, 2 - dayofweek - 7);
        return getDayStartTime(cal.getTime());
    }

    //获取上周的结束时间
    public static Date getEndDayOfLastWeek() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getBeginDayOfLastWeek());
        cal.add(Calendar.DAY_OF_WEEK, 6);
        Date weekEndSta = cal.getTime();
        return getDayEndTime(weekEndSta);
    }

    //获取本月的开始时间
    public static Date getBeginDayOfMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(getNowYear(), getNowMonth() - 1, 1);
        return getDayStartTime(calendar.getTime());
    }

    //获取本月的结束时间
    public static Date getEndDayOfMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(getNowYear(), getNowMonth() - 1, 1);
        int day = calendar.getActualMaximum(5);
        calendar.set(getNowYear(), getNowMonth() - 1, day);
        return getDayEndTime(calendar.getTime());
    }


    //获取N年前的开始时间
    public static Date getBeginDayOfNYearBefore(int n) {

        Calendar c = Calendar.getInstance();
        //过去n月
        c.setTime(new Date());
        c.add(Calendar.YEAR, -n);
        Date m = c.getTime();

//        Calendar calendar=Calendar.getInstance();
//        calendar.clear();
//        calendar.set(Calendar.YEAR, getYear(m));
        return getDayStartTime(getBeginDayOfYear(getYear(m)));
    }

    //获取N年前的结束时间
    public static Date getEndDayOfNYearBefore(int n) {
        Calendar c = Calendar.getInstance();
        //过去n年
        c.setTime(new Date());
        c.add(Calendar.YEAR, -n);
        Date m = c.getTime();

//        Calendar calendar = Calendar.getInstance();
//        calendar.clear();
//        calendar.set(Calendar.YEAR, getYear(m));
//        calendar.roll(Calendar.DAY_OF_YEAR,-1);
        return getDayEndTime(getEndDayOfYear(getYear(m)));
    }

    //获取上月的开始时间
    public static Date getBeginDayOfLastMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(getNowYear(), getNowMonth() - 2, 1);
        return getDayStartTime(calendar.getTime());
    }

    //获取上月的结束时间
    public static Date getEndDayOfLastMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(getNowYear(), getNowMonth() - 2, 1);
        int day = calendar.getActualMaximum(5);
        calendar.set(getNowYear(), getNowMonth() - 2, day);
        return getDayEndTime(calendar.getTime());
    }

    //获取本年的开始时间
    public static java.util.Date getBeginDayOfYear(Integer year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        // cal.set
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DATE, 1);

        return cal.getTime();
    }

    //获取本年的结束时间
    public static java.util.Date getEndDayOfYear(Integer year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DATE, 31);
        return cal.getTime();
    }

    //获取某个日期的开始时间
    public static Timestamp getDayStartTime(Date d) {
        Calendar calendar = Calendar.getInstance();
        if (null != d) calendar.setTime(d);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return new Timestamp(calendar.getTimeInMillis());
    }

    //获取某个日期的结束时间
    public static Timestamp getDayEndTime(Date d) {
        Calendar calendar = Calendar.getInstance();
        if (null != d) calendar.setTime(d);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return new Timestamp(calendar.getTimeInMillis());
    }

    //获取今年是哪一年
    public static Integer getNowYear() {
        Date date = new Date();
        GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
        gc.setTime(date);
        return Integer.valueOf(gc.get(1));
    }

    public static Integer getYear(Date date) {
        GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
        gc.setTime(date);
        return Integer.valueOf(gc.get(1));
    }

    //获取本月是哪一月
    public static int getNowMonth() {
        Date date = new Date();
        GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
        gc.setTime(date);
        return gc.get(2) + 1;
    }

    //两个日期相减得到的天数
    public static int getDiffDays(Date beginDate, Date endDate) {

        if (beginDate == null || endDate == null) {
            throw new IllegalArgumentException("getDiffDays param is null!");
        }

        long diff = (endDate.getTime() - beginDate.getTime())
                / (1000 * 60 * 60 * 24);

        int days = new Long(diff).intValue();

        return days;
    }

    //两个日期相减得到的毫秒数
    public static long dateDiff(Date beginDate, Date endDate) {
        long date1ms = beginDate.getTime();
        long date2ms = endDate.getTime();
        return date2ms - date1ms;
    }

    //获取两个日期中的最大日期
    public static Date max(Date beginDate, Date endDate) {
        if (beginDate == null) {
            return endDate;
        }
        if (endDate == null) {
            return beginDate;
        }
        if (beginDate.after(endDate)) {
            return beginDate;
        }
        return endDate;
    }

    //获取两个日期中的最小日期
    public static Date min(Date beginDate, Date endDate) {
        if (beginDate == null) {
            return endDate;
        }
        if (endDate == null) {
            return beginDate;
        }
        if (beginDate.after(endDate)) {
            return endDate;
        }
        return beginDate;
    }

    //返回某月该季度的第一个月
    public static Date getFirstSeasonDate(Date date) {
        final int[] SEASON = {1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int sean = SEASON[cal.get(Calendar.MONTH)];
        cal.set(Calendar.MONTH, sean * 3 - 3);
        return cal.getTime();
    }

    //返回某个日期下几天的日期
    public static Date getNextDay(Date date, int i) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.DATE, cal.get(Calendar.DATE) + i);
        return cal.getTime();
    }


    //返回某个日期前几天的日期
    public static Date getFrontDay(Date date, int i) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.DATE, cal.get(Calendar.DATE) - i);
        return cal.getTime();
    }

    public static Integer getHours(Date date){
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static String FORMAT_1 = "yyyy-MM-dd";
    public static String FORMAT_2 = "yyyy-MM-dd HH:mm:ss";
    public static String getDateFormat(Date date, String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }



    public static void main(String [] args){
//        System.out.println("=====" + getHours(new Date()));

        String START_NEWS = "test{0}";
        String s = MessageFormat.format(START_NEWS, "bbb");

        System.out.println(s);
    }

}
