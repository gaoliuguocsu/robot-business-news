package centaurstech.ai.robot.server.news.strategy.pre;

import centaurstech.ai.robot.server.common.result.QueryResult;
import centaurstech.ai.robot.server.news.pojo.CommonResult;
import centaurstech.ai.robot.server.news.pojo.NewsEntity;
import centaurstech.ai.robot.server.news.pojo.NewsResponse;
import centaurstech.ai.robot.server.news.pojo.api.LetingNews;
import centaurstech.ai.robot.server.news.pojo.cache.NewsCache;
import centaurstech.ai.robot.server.news.pojo.constants.FormVarName;
import centaurstech.ai.robot.server.news.pojo.constants.NewsContants;
import centaurstech.ai.robot.server.news.pojo.enums.ResultCode;
import centaurstech.ai.robot.server.news.pojo.query.NewsQuery;
import centaurstech.ai.robot.server.news.strategy.abstracts.StrategyResult;
import centaurstech.ai.robot.server.news.strategy.base.BaseStrategyHandler;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerName;
import centaurstech.ai.robot.server.news.strategy.enums.StrategyHandlerType;
import com.centaurstech.domain.FormRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class OperateNewsHandler extends BaseStrategyHandler {

    @Override
    public StrategyHandlerName getHandlerName() {
        return StrategyHandlerName.PRE_OPERATE_NEWS;
    }

    @Override
    public StrategyHandlerType getType() {
        return StrategyHandlerType.PRE;
    }

    @Override
    public StrategyResult<NewsQuery, NewsCache, CommonResult> apply(NewsQuery params, NewsCache cache) throws IOException {

        log.debug("操作处理器...");

        StrategyResult<NewsQuery, NewsCache, CommonResult> result = null;

        CommonResult commonResult = null;
        String value = params.get(FormVarName.VARS_OPERATE);
        if (StringUtils.isNotEmpty(value)){
            String [] operates = value.split("\\+");
            for (String operate : operates) {
                switch (operate) {
                    case "关闭":

                        String target = params.get(FormVarName.VARS_EARLY_EVENING);
                        if (NewsContants.EARLY_NEWS.equals(target)) {
                            //关闭早报
                            log.debug("关闭早报...");
                            return StrategyResult.finish(params, cache);
                        }

                    case "结束":
                    case "停止":
                    case "停":

                        //清除当前
                        memoryService.removeUserCache(cache.getChatKey());

                        String paramStr = "close";
                        commonResult = new CommonResult("", "", NewsContants.CLOSE_NEWS, paramStr);
                        result = StrategyResult.done(cache, commonResult);
                        return result;

                    case "播放":

                        if (cache == null || cache.getNewList() == null) {
                            params.put(FormVarName.VARS_NEWS, FormVarName.VARS_NEWS);
                            return StrategyResult.toNextPre(params, cache);
                        }

                        params.put(FormVarName.VARS_PREV_AND_NEXT, FormVarName.VALUE_GOON);

                        break;

                    case "暂停":
                        log.debug("暂停...");

                        commonResult = new CommonResult("", "", NewsContants.PAUSE_NEWS, "");
                        result = StrategyResult.done(cache, commonResult);
                        return result;

                    case "跳过":

                        params.put(FormVarName.VARS_PREV_AND_NEXT, FormVarName.VALUE_NEXT);
                        break;

                    case "点赞":

                        log.debug("点赞...");
                        commonResult = new CommonResult("", "", NewsContants.LIKE_NEWS, "");
                        result = StrategyResult.done(cache, commonResult);
                        return result;

                    case "分享":

                        log.debug("分享...");
                        commonResult = new CommonResult("", "", NewsContants.SHARE_NEWS, "");
                        result = StrategyResult.done(cache, commonResult);
                        return result;

                    case "设置":

                        String setTarget = params.get(FormVarName.VARS_EARLY_EVENING);
                        if (NewsContants.EARLY_NEWS.equals(setTarget)) {
                            //设置早报时间
                            log.debug("设置早报时间...");

                        } else if (NewsContants.EVENING_NEWS.equals(setTarget)) {
                            //设置晚报时间
                            log.debug("设置晚报时间...");

                        }

                        commonResult = new CommonResult("", "", NewsContants.SETTING_NEWS, "");
                        result = StrategyResult.done(cache, commonResult);
                        return result;

                    case "删除":
                    case "关了":

                        String delTarget = params.get(FormVarName.VARS_EARLY_EVENING);
                        if (StringUtils.isNotEmpty(delTarget)) {
                            if (NewsContants.EARLY_NEWS.equals(delTarget)) {
                                //删除早报时间
                                log.debug("删除早报时间...");

                            } else if (NewsContants.EVENING_NEWS.equals(delTarget)) {
                                //删除晚报时间
                                log.debug("删除晚报时间...");

                            }

                            commonResult = new CommonResult("", "", NewsContants.DELETE_NEWS, "");
                            result = StrategyResult.done(cache, commonResult);
                            return result;

                        }

                        break;

                    case "修改":
                    case "改一下":

                        String updTarget = params.get(FormVarName.VARS_EARLY_EVENING);
                        if (NewsContants.EARLY_NEWS.equals(updTarget)) {
                            //修改早报时间
                            log.debug("修改早报时间...");

                        } else if (NewsContants.EVENING_NEWS.equals(updTarget)) {
                            //修改晚报时间
                            log.debug("修改晚报时间...");

                        }

                        commonResult = new CommonResult("", "", NewsContants.UPDATE_NEWS, "");
                        result = StrategyResult.done(cache, commonResult);
                        return result;

                    case "查看":
                    case "查下":
                    case "看下":
                    case "看":
                    case "打开":

                        String function = params.get(FormVarName.VARS_FUNCTION);
                        if (NewsContants.FUNCTION_LIST.equals(function)) {
                            //查看列表
                            if (cache == null || cache.getNewList() == null) {
                                commonResult = new CommonResult("", "", NewsContants.EMPTY_LIST_NEWS, "");
                                result = StrategyResult.done(cache, commonResult);
                                return result;
                            }

                            List<LetingNews> list = cache.getNewList();
                            //将数据直接返回中控
                            NewsResponse respObj = new NewsResponse(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), NewsEntity.convert(list));
                            String s = objectMapper.writeValueAsString(respObj);
                            String ticket = chatApiService.sendNewsList(new JSONObject(s));

                            String newsStr = generateNews(list);
//                        String newsStr = list.get(index).getTitle();
                            commonResult = new CommonResult(NewsContants.FOUNDED_LIST_NEWS, newsStr, ticket);
                            result = StrategyResult.done(cache, commonResult);
                            return result;

                        }

                }

            }
        }



        String hot = params.get(FormVarName.VARS_HOT);
        if (StringUtils.isNotEmpty(hot)){
            params.put(FormVarName.VARS_NEWS_CATEGORY, "热点");
        }

//        String negate = params.get(FormVarName.VARS_NEGATE);
//        if (StringUtils.isNotEmpty(negate)){
//            String operate = params.get(FormVarName.VARS_OPERATE);
//            if (StringUtils.isNotEmpty(operate)){
//                switch (operate){
//                    case "听":
//                        String paramStr = "close";
//                        commonResult = new CommonResult("", "", NewsContants.CLOSE_NEWS, paramStr);
//                        result = StrategyResult.done(cache, commonResult);
//                        return result;
//                }
//            }
//
//        }


        //人称代词处理
        String personalPronoun = params.get(FormVarName.VARS_PERSONAL_PRONOUN);
        if (StringUtils.isNotEmpty(personalPronoun)){
            String name = cache.getNewsQuery().get(FormVarName.VARS_NAME);
            params.put(FormVarName.VARS_NAME, name);
        }

        //区域代词
        String areaPronoun = params.get(FormVarName.VARS_AREA_PRONOUN);
        if (StringUtils.isNotEmpty(areaPronoun)){
            String area = cache.getNewsQuery().get(FormVarName.VARS_AREA);
            params.put(FormVarName.VARS_AREA, area);
        }

        return StrategyResult.toNextPre(params, cache);
    }
}
