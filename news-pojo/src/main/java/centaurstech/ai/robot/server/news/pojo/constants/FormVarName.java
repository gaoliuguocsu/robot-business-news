package centaurstech.ai.robot.server.news.pojo.constants;

public class FormVarName {


    /**
     * bot相关
     */
    public final static String CHAT_KEY = "chatKey";
    public final static String UID = "uid";
    public final static String CHANNEL_ID = "channelId";
    public final static String QUERY_TEXT = "queryText";
    public final static String BOT_ACCOUNT = "botAccount";
    public final static String ORIGIN_TEXT = "originText";


    //资讯参数
    public final static String VARS_OPERATE = "操作";
    /**
     * 区域
     */
    public final static String VARS_AREA = "区域";

    public final static String PARAM_PROVINCE = "province";
    public final static String PARAM_CITY = "city";
    public final static String PARAM_AREA = "area";


    /**
     * 人名
     */
    public final static String VARS_NAME = "人名";

    /**
     * 类别
     */
    public final static String VARS_NEWS_CATEGORY = "新闻类别";


    /**
     * 时间
     */
    public final static String VARS_TIME_1 = "时间甲";
    public final static String VARS_TIME_2 = "时间乙";
    public final static String VARS_TIME_FRONT_BACK = "时间前后";

    public final static String PARAM_TIME_START = "time_start_param";
    public final static String PARAM_TIME_END = "time_end_param";
    public final static String PARAM_TIME_DATE = "time_date_param";
    public final static String PARAM_TIME_TYPE = "time_type_param";

    /**
     * 指定节目
     */
    public final static String VARS_NEWS_PROGRAM = "新闻节目";


    /**
     * 上一条 下一条 新闻
     */
    public final static String VARS_PREV_AND_NEXT = "上下条";
    public final static String VALUE_PREV = "上一条";
    public final static String VALUE_NEXT = "下一条";
    public final static String VALUE_GOON = "继续当前";

    /**
     * 推荐/热门
     */
    public final static String VARS_NEWS = "新闻";
    public final static String VARS_HOT = "热门";

    public final static String VARS_EVENT = "事件";
    public final static String VARS_NEGATE = "否定";
    public final static String VARS_PERSONAL_PRONOUN = "人称代词";
    public final static String VARS_FUNCTION = "功能";

    public final static String VARS_CHANGE = "更换";
    public final static String VARS_CHANGE_UNIT = "更换单位";
    public final static String VARS_AREA_PRONOUN = "区域代词";

    public final static String VARS_ORDER_NUM = "序数词";
    public final static String VARS_REVERSE = "倒序";

    public final static String VARS_EARLY_EVENING = "早晚报";


    public final static String PARAM_KEYWORD = "keyword";





}
